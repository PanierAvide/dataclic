# Develop

## Install your own instance

Dependencies : Linux, NodeJS (>= 14), PostgreSQL (>= 10), bash, unzip, gunzip, psql, xz, pg_tileserv, PostGIS

Note that other dependencies might be necessary according to defined themes.

```bash
git clone https://framagit.org/PanierAvide/dataclic.git
cd dataclic/
git submodule init
git submodule update
npm install
```

### Database

You should create a new PostgreSQL database, for example named `dataclic`, with the following extensions enabled : `unaccent`, `pg_trgm`.

Your PostgreSQL user should either be superadmin or be in `pg_read_server_files` group (you can set this using SQL request `GRANT pg_read_server_files TO your_user;`).

### Configuration file

Create a new `config.json` file, using for example provided template `config.example.json`. Following parameters should be set:

* `DB_NAME`: name of the PostgreSQL database (example `pdm`)
* `DB_HOST`: hostname of the PostgreSQL database (example `localhost`)
* `DB_PORT`: port number of the PostgreSQL database (example `5432`)
* `DATA_FOLDER`: path to a folder on disk where enough space is available for data storage and processing (several Gb depending to your themes, example `/home/user/dataclic`)
* `BASE_URL`: base URL to access the service, for metadata purposes (example `https://www.mywebsite.com`)
* `MAPTILER_API_KEY`: your maptiler api key (https://www.maptiler.com/)
* `TILESERV_URL`: the url of pg_tilserv (example `https://pg_tileserv.mywebsite.com`)

### Add mandatory base data

```bash
mkdir ./assets
cd assets
wget https://static.data.gouv.fr/resources/geozones/20190513-134438/geozones-france-2019-0-json.tar.xz
tar xvf geozones-france-2019-0-json.tar.xz
wget https://www.insee.fr/fr/statistiques/fichier/5057840/commune2021-csv.zip
unzip commune2021-csv.zip
mv commune2021.csv communes.csv
wget "https://datanova.laposte.fr/explore/dataset/laposte_hexasmal/download/?format=csv&timezone=Europe/Berlin&lang=fr&use_labels_for_header=true&csv_separator=%3B" -O laposte_hexasmal.csv
cd ..
npm run db:init
rm -rf assets
```

### Create your themes

Themes are thematic datasets configuration for both server processing and frontend display. They allow you to define what data should be retrieved and how to make it available. There are two kind of themes: `exposer` (offer national dataset as local dataset) and `converter` (transform user data and publish it).

Each theme is made available by creating a subfolder in `themes/` directory. In this subfolder, theme should have a `info.json` configuration file. Its content depend on the type of theme. Appart from this file, you can also define an optional `info.md` Markdown file, which gives more context about your theme (shown to users).

#### `exposer` themes

Exposer themes allows simplified extracts of national datasets. Configuration file `info.json` should have following properties:

* `type`: mandatory value `exposer`
* `name` : name of your theme (example `Entreprises`)
* `description`: short description of your theme (example `Téléchargez la liste SIRENE des entreprises de votre territoire.`)
* `style`: options for website display, with following sub-properties:
  * `icon`: FontAwesome icon to use (example `fa fa-building`)
  * `color`: background color for display (example `#AB47BC`)
* `metadata`: dataset metadata, with following sub-properties:
  * `dataset`: official name (example `Base SIRENE des entreprises et de leurs établissements`)
  * `producer`: name of dataset producer (example `Institut National de la Statistique et des Etudes Economiques`)
  * `doc`: URL to dataset documentation
  * `updates`: dataset updates frequency as described in `FREQ_MAJ` property in [SCDL Catalogue scheme](https://scdl.opendatafrance.net/docs/schemas/catalogue.html) (example `Mensuelle`)
  * `licence`: dataset licence (example `Licence Ouverte / Open Licence version 2.0`)
  * `category`: dataset category as described in `THEME` property in [SCDL Catalogue scheme](https://scdl.opendatafrance.net/docs/schemas/catalogue.html) (example `Économie`)
  * `precision`: dataset precision as described in `COUV_SPAT_MAILLE` property in [SCDL Catalogue scheme](https://scdl.opendatafrance.net/docs/schemas/catalogue.html) (example `Infracommunale`)
* `dataviz`: object (optional) containing definition of various data visualizations (either map or charts). See [DATAVIZ](./DATAVIZ.md) documentation for more information about their definition.
* `runner`: list of tasks to process dataset, task can be
  * `download`: example `{ "task": "download", "url": "https://files.data.gouv.fr/insee-sirene/StockEtablissement_utf8.zip" }`
  * `unzip`: example `{ "task": "unzip", "file": "StockEtablissement_utf8.zip" }`, works with .zip and .gz files. For zip files, you can add the parameter `only`, which should be an array containing the path to the files (as strings) you want to extract (the other files in the zip will not be extracted).
  * `convert_utf8`: example `{ "task": "convert_utf8", "file": "StockEtablissement_ISO-8859-1.txt", "encoding": "ISO-8859-1" }`   (encoding is ISO-8859-1 by default, use this parameter for others encodings)
  * `db_import`: example `{ "task": "db_import", "file": "StockEtablissement_utf8.csv", "format": "csv", "delimiter": ",", "header": true, "quote": "'", "table_name": "entreprises", "lowercase": true "adminMapping": true, "fusion": {"from": [ "﻿x", "y", "date_ech", "code_qual", "lib_qual" ], "to": ["*"]} }`
  * `sql`: runs a SQL file directly in database, example `{ "task": "sql", "file": "new_columns.sql" }`
  * `python`: runs a python script which should be located inside your theme folder, the working directory for the python script will be set to CONFIG.DATA_FOLDER (this is useful if you want to modify a file after it has been downloaded by the `download` task, without having to know what CONFIG.DATA_FOLDER is set to). Passing arguments and their values to the script is possible by adding them to the task (arguments without values are currently not supported), example `{ "task": "sql", "script": "my_script.py", "my_arg": "arg_value" }`
* `adminMapping`: mapping the selected dataset columns to administrative boundaries to create a link between your datasets and the administrative table, with following sub-properties:
  * `from`: array containing columns name in input dataset (example `["code_postal","commune"]`). Adding `~` as the first character of the column will map the selected dataset using word_similarities (example `["code_postal","~commune"]`).
  * `to`: corresponding table and column in administrative boundary reference table
    * `table` : table in administrative boundary (example `"commune"`)
    * `columns` : array containing the corresponding columns in administrative boundary reference table (example `[ "cp", "name" ]`)
* `table_links`: list of joins between the different datasets you imported in way to recup all of them in the output csv file, not needed if you only have one dataset, with following properties for each join:
  * `from`: first table name table and corresponding column (example `[ "entreprises", "codeCommuneEtablissement" ]`)
  * `to`: second table name and corresponding column (example `[ "ecoles", "codeCommuneEcole" ]`)

Available parameters for `download` tasks are :

* `url` : direct URL (http or https) of the file to download
* `file`: name given to the file on local filesystem (optional, by default name of file at given URL)

Available parameters for `db_import` tasks are :

* `file`: name of the file
* `format`: format of the file (must be `csv`, but task will work if your file can be parsed similarly to CSV)
* `delimiter`: delimiter of the CSV file (default `,`)
* `header`: true if the CSV file have an header (task don't work without a header)
* `header_line`: Optional, the line number of the header (count starts at 1)
* `quote`: quote caracter of the csv file (default `"`)
* `table_name`: name of the table that will be created in the DB (default theme name). In case of fusion, it's the table where your CSV rows will be inserted
* `lowercase`: true if the columns in the table need to be in lowercase (usefull for one char column names in tables because they are lowercased in the select by postgresql even with the "", but not in the table creation)
* `adminMapping`: if true, the adminMapping will be done on the table in the column referenced in the `from` sub-proprietie of the adminMapping propertie (default false)
* `fusion`: columns of the CSV file and the DB table that will be used t oinsert the CSV in the table, not needed if you just want to create a table with the CSV, with following sub-properties:
  * `from`: columns of the CSV file that will be inserted in the table (`["*"]` is special and select all columns of the file), lenght must be equal to the `to` parameter lenght, or <= to the number of the table columns if the `to` parameter is `"*"`
  * `to`: columns of the table that will recieve the CSV rows (`["*"]` is special and select all columns of the table)

#### `converter` themes

Converter themes allows user to convert their own dataset into standard formats. Theme folder can contain following files:

* `info.json`
* `info.md`
* `init.sh`: Bash script to initialize theme, for example install dependencies or import necessary external data
* `exec.sh`: Bash script to process user dataset

Configuration file `info.json` should contain following properties:

* `type`: mandatory value `converter`
* `name` : name of your theme (example `Budget`)
* `description`: short description of your theme (example `Transformez vos fichiers Totem XML en tableurs CSV anonymisés.`)
* `style`: options for website display, with following sub-properties:
  * `icon`: FontAwesome icon to use (example `fa fa-euro-sign`)
  * `color`: background color for display (example `#2196F3`)
* `parameters`: list of input parameters for `exec.sh` script, each having the following format:
  * `id`: name of parameter (only lowercase letters)
  * `name`: displayable name of parameter
  * `type`: kind of parameter (only `file` supported as now)
  * `format`: for `type=file`, MIME type of input dataset
* `output`: format of output file, with following sub-properties:
  * `format`: MIME type of output file (example `application/zip`)
  * `extension`: file extension for filename (example `zip`)
  * `scheme`: output file scheme (example `{ "name": "SCDL Budget", "url": "https://scdl.opendatafrance.net/docs/schemas/budget.html" }`)

The script `exec.sh` can do whatever processing is necessary. It should however follow some rules:

* Command-line arguments should be in same order as defined in `info.json`
* Temporary files should be stored as much as possible in `$DATA_FOLDER` path, and should be cleaned before exiting (even on failures)
* Name of output file should be printed in `stdout` using this syntax: `OUTPUT_FILE=/path/to/output.file`
* Progress made in script should be regularly printed in `stdout` with this syntax `PROGRESS=N` (`N` being an integer between 5 and 99, representing progress in percent)


### Initialize themes

Once configuration of themes is done, you can run the following command to prepare each theme:

```bash
node src/runner.js init <YOUR_THEME>
```

### Run website

```bash
npm run start
```

### Regular tasks / maintenance

Some tasks should run regularly to keep your instance working smoothly. Add in a cron following commands:

```
# Remove temporary files from failed downloads attempts
0 1 * * * find /path/to/data/dir -mtime +7 -type f -delete
```

Depending on your themes, you might also need to run regularly `node src/runner.js init <YOUR_THEME>` to update datasets.
