/**
 * Runner code : launches data retrieval or processing
 */

const fs = require('fs');
const { createGunzip } = require('node:zlib');
const { pipeline } = require('node:stream');
const spawn = require('child_process').spawn;
const themes = require('./themes');
const { download, getDbClient, getUrlFileName, ADMIN_TYPES, CONFIG } = require('./utils');
const { Client } = require('pg');
const copyTo = require('pg-copy-streams').to;
const csvparse = require('csv-parse');
const { join } = require('path');


/**
 * Prepares data and tooling to make given theme usable
 * @param {string} themeId The theme ID
 * @return {Promise}
 */
function initTheme(themeId) {
	if(!themes[themeId]) {
		throw new Error("Invalid theme ID: "+themeId);
	}

	const theme = themes[themeId];

	switch(theme.type) {
		case "exposer":
			return initExposerTheme(theme);
		case "converter":
			return initConverterTheme(theme);
		default:
			throw new Error("Unsupported theme type: "+theme.type);
	}
}

function initExposerTheme(theme) {
	const tasks = theme.runner.slice(0);
	const runNextTask = () => new Promise((resolve, reject) => {
		const currentTask = tasks.shift();
		if(currentTask) {
			switch(currentTask.task) {
				case "download":
					var url = currentTask.url;
					if (currentTask.parameters) {
						Object.keys(currentTask.parameters).forEach((key) => {
							const param = currentTask.parameters[key];
							var value = param.value;
							if (param.replace) {
								Object.keys(param.replace).forEach((fun_name) => {
									const f = eval(param.replace[fun_name]);
									const regex = RegExp(fun_name, 'g');
									value = value.replace(regex, f());
								})
							}
							const regex = RegExp(key, 'g');
							url = url.replace(regex, encodeURIComponent(value));
						});
					}

					const filename = currentTask.file || getUrlFileName(currentTask.url);
					download(url, CONFIG.DATA_FOLDER+'/'+filename, true)
					.then(() => runNextTask())
					.catch(e => reject(e));
					break;

				case "python":
					// Create args list
					const args = [process.cwd() + "/themes/" + theme.id + "/" + currentTask.script]
					Object.entries(currentTask).forEach(([key, value]) => {
						if ("task" !== key && "script" !== key) {
							args.push("--" + key);
							args.push(value);
						}
					});
					const cmd = spawn("python3", args, {cwd: CONFIG.DATA_FOLDER });
					cmd.stdout.on("data", data => console.log(data.toString()));
					cmd.stderr.on("data", data => console.error(data.toString()));
					cmd.on("exit", code => code === 0 ? runNextTask() : reject("Python task failed"));
					break;

				case "unzip":
					const input_file = CONFIG.DATA_FOLDER + '/' + currentTask.file;

					if (currentTask.file.endsWith(".gz")) {
						const source = fs.createReadStream(input_file);
						const extractor = createGunzip();
						const destination = fs.createWriteStream(CONFIG.DATA_FOLDER + '/' + currentTask.file.slice(0, -3));
						pipeline(source, extractor, destination, (err) => {
							if (err) {
								console.log(err);
								reject("Gunzip failed");
							} else {
								runNextTask();
							}
						});
					} else {
						var cmd_args;
						if (currentTask["only"]) {
							cmd_args = ["-j", input_file, ...currentTask.only, "-d", CONFIG.DATA_FOLDER];
						} else {
							cmd_args = ["-o", input_file, "-d", CONFIG.DATA_FOLDER];
						}
						const bash = spawn('unzip', cmd_args);
						bash.stdout.on("data", data => console.log(data.toString()));
						bash.stderr.on("data", data => console.error(data.toString()));
						bash.on("exit", code => code === 0 ? runNextTask() : reject("Unzip failed"));
					}

					break;

				case "convert_utf8":
					var input = fs.readFileSync(CONFIG.DATA_FOLDER+'/'+currentTask.file, {encoding: "binary"});
					var iconv = require('iconv-lite');
					var encoding = "ISO-8859-1";
					if (currentTask.encoding) {
						encoding=currentTask.encoding;
					}
					var output = iconv.decode(Buffer.from(input), encoding);
					fs.writeFileSync(CONFIG.DATA_FOLDER+"/"+currentTask.file, output);
					console.log("Convertion completed");
					runNextTask();
					break;

				case "db_import":
					if(currentTask.format === "csv" && currentTask.header) {
						// Read columns from CSV file
						let columns = null;
						fs.createReadStream(CONFIG.DATA_FOLDER+'/'+currentTask.file)
						.pipe(csvparse.parse({
							delimiter: currentTask.delimiter,
							columns: true,
							relax_quotes: true,
							from_line: currentTask.header_line ? currentTask.header_line : undefined,
						}))
						.on('data', (row) => {
							columns = Object.keys(row);
						})
						.on('error', err => reject(err))
						.on('end', () => {
							if(!columns) { return reject("CSV header read failed"); }

							// List SQL requests to run
							let sqlQueries = getSqlQueriesForTheme(theme, columns, currentTask);

							// Launch queries
							console.log("Connecting to database");
							const db = getDbClient();
							db.connect().then(() => {
								const runNextQuery = () => {
									const sql = sqlQueries.shift();
									if(sql) {
										console.log("SQL:", sql.split(" ").slice(0, 2).join(" "));
										db.query(sql).then(() => runNextQuery())
										.catch(e => {
											db.end();
											reject(e);
										});
									}
									else {
										db.end();
										runNextTask();
									}
								};
								return runNextQuery();
							})
							.catch(e => reject(e));
						});
					}
					else {
						reject("Unsupported format for DB import: "+currentTask.format);
					}
					break;

				case "sql":
					const query_str = fs.readFileSync("./themes/" + theme.id + "/" + currentTask.file).toString();
					const db = getDbClient();
					db.connect().then(() => {
						db.query(query_str, [])
							.then(() => {
								db.end();
								runNextTask();
							})
							.catch(e => {
								db.end();
								reject(e);
							});

					}).catch(e => reject(e));;
					break;

				default:
					if(currentTask._task) {
						console.log("Skip disabled task", currentTask._task);
						runNextTask();
					}
					else {
						reject("Unsupported task type: "+currentTask.task);
					}
			}
		}
		else {
			// Cleanup temporary files
			theme.runner.forEach(task => {
				let file = task.file;
				if(!file && task.url) {
					file = task.url.split('/').pop().split('#')[0].split('?')[0];
				}
				file = CONFIG.DATA_FOLDER+'/'+file;
				fs.unlink(file, (err) => {;});
			});
			initDataviz(theme).then(() => resolve());
		}
	});

	return runNextTask();
}


function initConverterTheme(theme) {
	return new Promise((resolve, reject) => {
		const initScript = `${__dirname}/../themes/${theme.id}/init.sh`;
		if(fs.existsSync(initScript)) {
			const bash = spawn('bash', [initScript], { env: { DATA_FOLDER: CONFIG.DATA_FOLDER } });

			bash.stdout.on("data", data => console.log(data.toString()));
			bash.stderr.on("data", data => console.error(data.toString()));
			bash.on("exit", code => code === 0 ? resolve() : reject("Init script for theme "+theme.id+" failed"));
		}
		else {
			console.log("Skip init for theme "+theme.id);
			resolve();
		}
	});
}


/**
 * Launches query to retrieve wanted data in selected theme
 * @param {string} themeId The theme ID
 * @param {string[]} options Parameters for extract
 * @return {Promise} Resolves on output file name
 */
function queryTheme(themeId, options) {
	if(!themes[themeId]) {
		throw new Error("Invalid theme ID: "+themeId);
	}

	const theme = themes[themeId];

	switch(theme.type) {
		case "exposer":
			return queryExposerTheme(theme, options);
		case "converter":
			return queryConverterTheme(theme, options);
		default:
			throw new Error("Unsupported theme type: "+theme.type);
	}
}

function queryExposerTheme(theme, options, res) {
	const [ adminType, adminCode ] = options;
	if(!ADMIN_TYPES.includes(adminType)) { throw new Error("Invalid admin type: "+adminType+" (should be one of "+ADMIN_TYPES.join(", ")+")"); }
	if(!/^[A-Za-z0-9_-]+$/.test(adminCode)) { throw new Error("Invalid admin code: "+adminCode+" (only alphanumeric characters allowed)"); }

	const outFile = `${CONFIG.DATA_FOLDER}/${adminType}_${adminCode}_${Date.now()}.csv`;

	var tables = theme.id;

	if (theme.tables_links){
		tables = "";

		for (var i = 0; i < theme.tables_links.length ; i++) {
			if (i === 0) {
				tables += theme.tables_links[i].from[0];
			}
			tables += ` LEFT JOIN ${theme.tables_links[i].to[0]} ON ${theme.tables_links[i].from[0]}.\"${theme.tables_links[i].from[1]}\" = ${theme.tables_links[i].to[0]}.\"${theme.tables_links[i].to[1]}\" `;
		}
	}

	const sql = `COPY (SELECT * FROM ${tables} WHERE x_${adminType}_code = '${adminCode}') TO STDOUT CSV HEADER`;

	const db = getDbClient();
	return db.connect()
	.then(() => new Promise((resolve, reject) => {
		const stream = db.query(copyTo(sql));
		stream.pipe(res || fs.createWriteStream(outFile));
		stream.on("end", resolve);
		stream.on("error", reject);
	}))
	.then(() => {
		return res ? null : outFile;
	})
	.catch(e => {
		throw e;
	})
	.finally(() => db.end());
}

function hasDataExposerTheme(theme, options) {
	const [ adminType, adminCode ] = options;
	if(!ADMIN_TYPES.includes(adminType)) { throw new Error("Invalid admin type: "+adminType+" (should be one of "+ADMIN_TYPES.join(", ")+")"); }
	if(!/^[A-Za-z0-9_-]+$/.test(adminCode)) { throw new Error("Invalid admin code: "+adminCode+" (only alphanumeric characters allowed)"); }

	const theme_properties = themes[theme.id];

	var table = theme.id;

	for (var i = 0; i < theme_properties.runner.length ; i++) {
		if (theme_properties.runner[i].adminMapping && theme_properties.runner[i].table_name) {
			table = theme_properties.runner[i].table_name;
			break;
		}
	}

	const sql = `SELECT * FROM ${table} WHERE x_${adminType}_code = '${adminCode}' LIMIT 1`;

	const db = getDbClient();
	return db.connect()
	.then(() => {
		return db.query(sql);
	})
	.then(res => {
		if (res.rowCount > 0) {
			return true;
		}
		return false;
	})
	.catch(e => {
		throw e;
	})
	.finally(() => db.end());
}

function queryConverterTheme(theme, options, progressCallback) {
	progressCallback = progressCallback || (() => {;});

	return new Promise((resolve, reject) => {
		const execScript = `${__dirname}/../themes/${theme.id}/exec.sh`;
		const bash = spawn('bash', [execScript, ...options], { env: { DATA_FOLDER: CONFIG.DATA_FOLDER } });
		let bashOutput = null;

		bash.stdout.on("data", data => {
			const txt = data.toString();
			console.log(txt);

			let progressTxt = txt.split("\n").map(l => l.trim()).filter(l => l.startsWith("PROGRESS=")).pop();
			if(progressTxt && progressTxt.split("=").length === 2) { progressCallback("running", progressTxt.split("=")[1]); }

			bashOutput = txt.split("\n").filter(l => l.trim().length > 0).pop(); // Keep track of last line to find output file
		});
		bash.stderr.on("data", data => console.error(data.toString()));
		bash.on("exit", code => {
			if(code === 0) {
				if(bashOutput && bashOutput.startsWith("OUTPUT_FILE=")) {
					const file = bashOutput.substring(12);
					if(fs.existsSync(file)) {
						progressCallback("ready", { servername: file, extension: theme.output.extension, mime: theme.output.format });
						resolve(file);
					}
					else {
						const msg = "Converter didn't produce proper file result (it's not your fault, it's ours)";
						progressCallback("error", msg);
						reject(msg);
					}
				}
				else {
					const msg = "Converter didn't give as output the result file name (it's not your fault, it's ours)";
					progressCallback("error", msg);
					reject(msg);
				}
			}
			else {
				const msg = "Converter script didn't work as expected (it's not your fault, it's ours)";
				progressCallback("error", msg);
				reject(msg);
			}
		});
	});
}

/**
 * Generates list of SQL queries for theme init.
 */
function getSqlQueriesForTheme(theme, columns, task) {
	var table_name = theme.id;
	if(task.table_name){
		table_name = task.table_name;
	}

	var adminMapping = false;
	if (task.adminMapping){
		adminMapping=task.adminMapping;
	}

	if (task.lowercase) {
		columns = columns.map(v => v.toLowerCase());
	}

	if(adminMapping) {
		// Next table creation + data import
		if (task.fusion) {
			var queries = getSQLQueriesForFusion(columns, task, table_name);
			queries.push(`ALTER TABLE ${table_name} RENAME TO ${table_name}_next`);
		}
		else {
			var queries = [
				`DROP TABLE IF EXISTS ${table_name}_next`,
				`CREATE TABLE ${table_name}_next (${columns.map(c => `"${c}" VARCHAR`).join(", ")})`,
				`COPY ${table_name}_next FROM '${CONFIG.DATA_FOLDER+'/'+task.file}' CSV HEADER DELIMITER '${task.delimiter || ","}' QUOTE '${task.quote || "\""}'`
			];
		}

		// Array size of from and to in info.json
		const from_len = theme.adminMapping.from.length;
		const to_len = theme.adminMapping.to.columns.length;

		// Check if the number of arguments(columns) are the same
		if(from_len != to_len) throw "The number of columns doesn't match"

		// Append administrative levels info to table
		switch(theme.adminMapping.to.table) {
			case "communes":
				// Building LEFT JOIN query based on the number of columns indicated by the user
				// Using word_similarity if start with ~
				const condition = [];
				var index_query = "";
				var select_query = "";
				var cp_query = null;

				for(var i = 0; i < from_len; i++)
				{
					// Special case for postal code since it has its own table
					if(theme.adminMapping.to.columns[i] == "cp")
					{
						var column = theme.adminMapping.from[i];
						select_query = ",communes_cp.Code_postal as x_commune_code_postal, communes_cp.Code_commune_INSEE as x_commune_code_insee";
						cp_query = `JOIN communes_cp ON communes_cp.Code_commune_INSEE = c.code AND communes_cp.Code_postal = a."${column}"`;
					}
					else
					{
						if(theme.adminMapping.from[i].startsWith('~'))
						{
							var column = theme.adminMapping.from[i].substring(1);
							index_query = index_query.concat(" ",`CREATE INDEX "${table_name}_next_${column}_idx" ON ${table_name}_next("${column}");`);
							condition.push(`word_similarity(c.${theme.adminMapping.to.columns[i]},a."${column}") >= 0.8`);
						}
						else
						{
							var column = theme.adminMapping.from[i];
							index_query = index_query.concat(" ",`CREATE INDEX "${table_name}_next_${column}_idx" ON ${table_name}_next("${column}");`);
							condition.push(`lower(c.${theme.adminMapping.to.columns[i]}) = lower(a."${column}") `);
						}
					}
				}

				// Add "Left join"
				const left_join_query = "LEFT JOIN communes c ON";
				condition[0] = left_join_query.concat(" ",condition[0])

				// Add "AND"
				join_query = condition.join(" AND ");

				// Add postal code query
				if(cp_query !== null)
					join_query = join_query.concat(" ",cp_query);

				// Add index query
				if(index_query !== "")
					queries.push(index_query)

				queries = queries.concat([
					`CREATE TABLE ${table_name}_next_full AS
						SELECT
							a.*, c.code AS x_commune_code, c.name AS x_commune_name,
							c.epci AS x_epci_code, c.epci_name AS x_epci_name,
							c.arrondissement AS x_arrondissement_code, c.arrondissement_name AS x_arrondissement_name,
							c.departement AS x_departement_code, c.departement_name AS x_departement_name,
							c.region AS x_region_code, c.region_name AS x_region_name
							${select_query}
						FROM ${table_name}_next a
						${join_query}`,
					`DROP TABLE ${table_name}_next`,
					`ALTER TABLE ${table_name}_next_full RENAME TO ${table_name}_next`,
					`CREATE INDEX ${table_name}_next_x_commune_code_idx ON ${table_name}_next(x_commune_code)`,
					`CREATE INDEX ${table_name}_next_x_epci_code_idx ON ${table_name}_next(x_epci_code)`,
					`CREATE INDEX ${table_name}_next_x_arrondissement_code_idx ON ${table_name}_next(x_arrondissement_code)`,
					`CREATE INDEX ${table_name}_next_x_departement_code_idx ON ${table_name}_next(x_departement_code)`,
					`CREATE INDEX ${table_name}_next_x_region_code_idx ON ${table_name}_next(x_region_code)`
				]);

				break;


			case "epcis":
				queries = queries.concat([
					`ALTER TABLE ${table_name}_next ADD COLUMN x_epci_code VARCHAR`,
					`ALTER TABLE ${table_name}_next ADD COLUMN x_epci_name VARCHAR`,
					`CREATE INDEX "${table_name}_next_${theme.adminMapping.from}_idx" ON ${table_name}_next("${theme.adminMapping.from}")`,
					`CREATE TABLE ${table_name}_next_full AS
						SELECT
							a.*, e.code AS x_epci_code, e.name AS x_epci_name
						FROM ${table_name}_next a
						LEFT JOIN epcis e ON e.${theme.adminMapping.to[1]} = a."${theme.adminMapping.from}"`,
					`DROP TABLE ${table_name}_next`,
					`ALTER TABLE ${table_name}_next_full RENAME TO ${table_name}_next`,
					`CREATE INDEX ${table_name}_next_x_epci_code_idx ON ${table_name}_next(x_epci_code)`
				]);
				break;

			case "arrondissements":
				queries = queries.concat([
					`ALTER TABLE ${table_name}_next ADD COLUMN x_arrondissement_code VARCHAR`,
					`ALTER TABLE ${table_name}_next ADD COLUMN x_arrondissement_name VARCHAR`,
					`CREATE INDEX "${table_name}_next_${theme.adminMapping.from}_idx" ON ${table_name}_next("${theme.adminMapping.from}")`,
					`CREATE TABLE ${table_name}_next_full AS
						SELECT
							a.*, ar.code AS x_arrondissement_code, ar.name AS x_arrondissement_name
						FROM ${table_name}_next a
						LEFT JOIN arrondissements ar ON ar.${theme.adminMapping.to[1]} = a."${theme.adminMapping.from}"`,
					`DROP TABLE ${table_name}_next`,
					`ALTER TABLE ${table_name}_next_full RENAME TO ${table_name}_next`,
					`CREATE INDEX ${table_name}_next_x_arrondissement_code_idx ON ${table_name}_next(x_arrondissement_code)`
				]);
				break;

			case "departements":
				queries = queries.concat([
					`ALTER TABLE ${table_name}_next ADD COLUMN x_departement_code VARCHAR`,
					`ALTER TABLE ${table_name}_next ADD COLUMN x_departement_name VARCHAR`,
					`ALTER TABLE ${table_name}_next ADD COLUMN x_region_code VARCHAR`,
					`ALTER TABLE ${table_name}_next ADD COLUMN x_region_name VARCHAR`,
					`CREATE INDEX "${table_name}_next_${theme.adminMapping.from}_idx" ON ${table_name}_next("${theme.adminMapping.from}")`,
					`CREATE TABLE ${table_name}_next_full AS
						SELECT
							a.*, d.code AS x_departement_code, d.name AS x_departement_name,
							d.region AS x_region_code, d.region_name AS x_region_name
						FROM ${table_name}_next a
						LEFT JOIN departements d ON d.${theme.adminMapping.to[1]} = a."${theme.adminMapping.from}"`,
					`DROP TABLE ${table_name}_next`,
					`ALTER TABLE ${table_name}_next_full RENAME TO ${table_name}_next`,
					`CREATE INDEX ${table_name}_next_x_departement_code_idx ON ${table_name}_next(x_departement_code)`,
					`CREATE INDEX ${table_name}_next_x_region_code_idx ON ${table_name}_next(x_region_code)`
				]);
				break;

			case "regions":
				queries = queries.concat([
					`ALTER TABLE ${table_name}_next ADD COLUMN x_region_code VARCHAR`,
					`ALTER TABLE ${table_name}_next ADD COLUMN x_region_name VARCHAR`,
					`CREATE INDEX "${table_name}_next_${theme.adminMapping.from}_idx" ON ${table_name}_next("${theme.adminMapping.from}")`,
					`CREATE TABLE ${table_name}_next_full AS
						SELECT
							a.*, r.code AS x_region_code, r.name AS x_region_name
						FROM ${table_name}_next a
						LEFT JOIN regions r ON r.${theme.adminMapping.to[1]} = a."${theme.adminMapping.from}"`,
					`DROP TABLE ${table_name}_next`,
					`ALTER TABLE ${table_name}_next_full RENAME TO ${table_name}_next`,
					`CREATE INDEX ${table_name}_next_x_region_code_idx ON ${table_name}_next(x_region_code)`
				]);
				break;

			default:
				throw new Error("Unrecognized administrative table: "+theme.adminMapping.to.table);
		}

		// Table swap
		queries = queries.concat([
			`DROP TABLE IF EXISTS ${table_name} CASCADE`,
			`ALTER TABLE ${table_name}_next RENAME TO ${table_name}`
		]);

		// Rename indexes
		switch(theme.adminMapping.to.table) {
			case "communes":
				queries = queries.concat([
					`ALTER INDEX ${table_name}_next_x_commune_code_idx RENAME TO ${table_name}_x_commune_code_idx`,
					`ALTER INDEX ${table_name}_next_x_epci_code_idx RENAME TO ${table_name}_x_epci_code_idx`,
					`ALTER INDEX ${table_name}_next_x_arrondissement_code_idx RENAME TO ${table_name}_x_arrondissement_code_idx`,
					`ALTER INDEX ${table_name}_next_x_departement_code_idx RENAME TO ${table_name}_x_departement_code_idx`,
					`ALTER INDEX ${table_name}_next_x_region_code_idx RENAME TO ${table_name}_x_region_code_idx`
				]);
				break;

			case "epcis":
				queries.push(`ALTER INDEX ${table_name}_next_x_epci_code_idx RENAME TO ${table_name}_x_epci_code_idx`);
				break;

			case "arrondissements":
				queries.push(`ALTER INDEX ${table_name}_next_x_arrondissement_code_idx RENAME TO ${table_name}_x_arrondissement_code_idx`);
				break;

			case "departements":
				queries = queries.concat([
					`ALTER INDEX ${table_name}_next_x_departement_code_idx RENAME TO ${table_name}_x_departement_code_idx`,
					`ALTER INDEX ${table_name}_next_x_region_code_idx RENAME TO ${table_name}_x_region_code_idx`
				]);
				break;

			case "regions":
				queries.push(`ALTER INDEX ${table_name}_next_x_region_code_idx RENAME TO ${table_name}_x_region_code_idx`);
				break;

			default:
				throw new Error("Unrecognized administrative table: "+theme.adminMapping.to.table);
			}
	}

	else {
		if (task.fusion) {
			var queries = getSQLQueriesForFusion(columns, task, table_name);
		}
		else {
			var queries = [
				`DROP TABLE IF EXISTS ${table_name} CASCADE`,
				`CREATE TABLE ${table_name} (${columns.map(c => `"${c}" VARCHAR`).join(", ")})`,
				`COPY ${table_name} FROM '${CONFIG.DATA_FOLDER+'/'+task.file}' CSV HEADER DELIMITER '${task.delimiter || ","}' QUOTE '${task.quote || "\""}'`
			];
		}
	}

	queries.push(`INSERT INTO themes(id, last_update) VALUES('${table_name}', now()) ON CONFLICT (id) DO UPDATE SET last_update = EXCLUDED.last_update`);

	return queries;
}

function getSQLQueriesForFusion(columns, task, table_name) {
	var queries = [
		`DROP TABLE IF EXISTS ${table_name}_insert`,
		`CREATE TABLE ${table_name}_insert (${columns.map(c => `"${c}" VARCHAR`).join(", ")})`,
		`COPY ${table_name}_insert FROM '${CONFIG.DATA_FOLDER+'/'+task.file}' CSV HEADER DELIMITER '${task.delimiter || ","}' QUOTE '${task.quote || "\""}'`
	];
	var query = `INSERT INTO ${table_name} `;
	if (task.fusion.to[0]!="*"){
		query += `(${task.fusion.to.map(c => `"${c}"`).join(", ")}) `
	}
	if (task.fusion.from[0]=="*"){
		query += `SELECT * FROM ${table_name}_insert`;
	}
	else {
		query += `SELECT ${task.fusion.from.map(c => `"${c}"`).join(", ")} FROM ${table_name}_insert`;
	}
	queries.push(query);
	queries.push(`DROP TABLE IF EXISTS ${table_name}_insert`);
	return queries;
}

function initDataviz(theme) {
	const commune_query = `
		drop materialized view if exists <TABLE>_<MAPKEY>_commune cascade;
		create materialized view <TABLE>_<MAPKEY>_commune as
			with
				total_query as (
					select
						count(*) as total,
						x_commune_code,
						x_epci_code,
						x_arrondissement_code,
						x_departement_code,
						x_region_code
					from <TABLE>
					group by
						x_commune_code,
						x_epci_code,
						x_arrondissement_code,
						x_departement_code,
						x_region_code
				), geom_query as (
					select
						code,
						name as geom_name,
						<GEOM_COLUMN> as geom
					from communes
					group by code, name
				)
			select
				total_query.total,
				geom_query.geom_name,
				geom_query.geom,
				x_commune_code,
				x_epci_code,
				x_arrondissement_code,
				x_departement_code,
				x_region_code
			from total_query
			inner join geom_query on geom_query.code = total_query.x_commune_code
			group by
				x_commune_code,
				total_query.total,
				geom_query.geom,
				geom_query.geom_name,
				x_epci_code,
				x_arrondissement_code,
				x_departement_code,
				x_region_code
		;
		CREATE INDEX "<TABLE>_<MAPKEY>_commune.geom_idx" ON <TABLE>_<MAPKEY>_commune USING gist (geom);
		CREATE INDEX "<TABLE>_<MAPKEY>_commune.x_commune_code_idx" ON <TABLE>_<MAPKEY>_commune (x_commune_code);
		CLUSTER <TABLE>_<MAPKEY>_commune USING "<TABLE>_<MAPKEY>_commune.geom_idx";
	`;
	const adm_query = `
		drop materialized view if exists <TABLE>_<MAPKEY>_<ADMTYPE> cascade;
		create materialized view <TABLE>_<MAPKEY>_<ADMTYPE> as
		with
			total_query as (
				select
					sum(total) as total,
					x_<ADMTYPE>_code as code
				from <TABLE>_<MAPKEY>_commune
				WHERE x_<ADMTYPE>_code is not null
				group by x_<ADMTYPE>_code
			),
			geom_query as (
				select
					code,
					name as geom_name,
					<GEOM_COLUMN> as geom
				from <ADMTYPE>s
				group by code, name
			)
		select
			<PREVIOUS_ADMTYPES_CODE>
			total_query.total,
			total_query.code as x_<ADMTYPE>_code,
			geom_query.geom,
			geom_query.geom_name
		from total_query
		inner join geom_query on geom_query.code = total_query.code
		left join communes on <ADMTYPE> = total_query.code
		group by
			<PREVIOUS_ADMTYPES>
			total_query.code,
			total_query.total,
			geom_query.geom,
			geom_query.geom_name
		;
		<INDEXES>
		CREATE INDEX "<TABLE>_<MAPKEY>_<ADMTYPE>.geom_idx" ON <TABLE>_<MAPKEY>_<ADMTYPE> USING gist (geom);
		CREATE INDEX "<TABLE>_<MAPKEY>_<ADMTYPE>.x_<ADMTYPE>_code_idx" ON <TABLE>_<MAPKEY>_<ADMTYPE> (x_<ADMTYPE>_code);
		CLUSTER <TABLE>_<MAPKEY>_<ADMTYPE> USING "<TABLE>_<MAPKEY>_<ADMTYPE>.geom_idx";
	`;
	const function_query = `
		CREATE OR REPLACE
		FUNCTION public.<TABLE>_<MAPKEY>_cluster(z integer, x integer, y integer)
		RETURNS bytea
		AS $$
		DECLARE
			result bytea;
		BEGIN
			WITH
			bounds AS (
			SELECT ST_Transform(ST_TileEnvelope(z, x, y), 4326) AS geom
			),
			points AS(
				select
					<COLUMNS>
					v.geom as geom
				from <TABLE> v, bounds
				where ST_Contains(bounds.geom, v.geom)
			),
			clusters as (
				select
					<COLUMNS>
					ST_ClusterDBSCAN(geom, eps := z/20000.0, minpoints := 2) over () AS cid, geom
				from points
			),
			mvtgeom AS (
				select
					<COALESCE_COLUMNS>
					ST_AsMVTGeom(ST_centroid(ST_Collect(clusters.geom)), bounds.geom) AS geom,
					count(*) as total
				from clusters, bounds
				group by <COALESCE_GROUP> bounds.geom, cid
			)
			SELECT ST_AsMVT(mvtgeom, 'public.<TABLE>_<MAPKEY>_cluster')
			INTO result
			FROM mvtgeom;

			RETURN result;
		END;
		$$
		LANGUAGE 'plpgsql'
		STABLE
		PARALLEL SAFE;
	`;
	const map_ids = theme.dataviz ? Object.keys(theme.dataviz) : [];
	const runNextMap = () => new Promise((resolve_maps, reject_map) => {
		const map_id = map_ids.shift();
		if (!map_id)
			return resolve_maps();
		const map_config = theme.dataviz[map_id];

		var geom_column;
		if (map_config.subtype == "choropleth")
			geom_column = "geo_boundary";
		else if (map_config.subtype == "cluster")
			geom_column = "geo_center";
		else
			return runNextMap();

		var admtypes = ADMIN_TYPES.slice(0);
		if (map_config.subtype == 'cluster')
			admtypes = ['cluster', ...admtypes];

		const runNextQuery = () => new Promise((resolve_queries, reject_query) => {
			const admtype = admtypes.shift();
			if (!admtype) {
				resolve_queries();
				runNextMap();
			} else {
				const db = getDbClient();
				db.connect().then(() => {
					var query_str;
					if (admtype == "commune") {
						query_str = commune_query
							.replace(/<TABLE>/g, map_config.table)
							.replace(/<MAPKEY>/g, map_id)
							.replace(/<GEOM_COLUMN>/g, geom_column);
					} else if (admtype == 'cluster') {
						var columns = '';
						var coalesce_columns = '';
						var coalesce_group = '';
						if (map_config.popup && map_config.popup.length > 0) {
							columns = map_config['popup'].map(e => e.column).join() + ',';
							coalesce_columns = map_config.popup.map(e => `coalesce(cid::varchar, ${e.column}) as ${e.column}`).join() + ',';
							coalesce_group = map_config.popup.map(e => `coalesce(cid::varchar, ${e.column})`).join() + ',';
						}
						query_str = function_query
							.replace(/<TABLE>/g, map_config.table)
							.replace(/<MAPKEY>/g, map_id)
							.replace(/<COLUMNS>/g, columns)
							.replace(/<COALESCE_COLUMNS>/g, coalesce_columns)
							.replace(/<COALESCE_GROUP>/g, coalesce_group);
					} else {
						var indexes = '';
						var previous_admtypes = '';
						var previous_admtypes_code = '';
						if (admtypes.length > 0) {
							indexes = admtypes.map(e => `CREATE INDEX "<TABLE>_<MAPKEY>_${admtype}.x_${e}_code_idx" ON <TABLE>_<MAPKEY>_${admtype} (x_${e}_code);`).join(' ');
							previous_admtypes = admtypes.join() + ',';
							previous_admtypes_code = admtypes.map(e => `${e} as x_${e}_code`).join() + ',';
						}
						query_str = adm_query
							.replace(/<INDEXES>/g, indexes)
							.replace(/<TABLE>/g, map_config.table)
							.replace(/<MAPKEY>/g, map_id)
							.replace(/<ADMTYPE>/g, admtype)
							.replace(/<GEOM_COLUMN>/g, geom_column)
							.replace(/<PREVIOUS_ADMTYPES>/g, previous_admtypes)
							.replace(/<PREVIOUS_ADMTYPES_CODE>/g, previous_admtypes_code);
					}
					db.query(query_str, []).then(() => {
						db.end();
						console.log(`Created view ${admtype} for map ${map_id}`);
						runNextQuery();
					}).catch(e => {
						db.end();
						reject_query(e);
					});
				}).catch(e => reject_query(e));
			}
		});
		runNextQuery().catch(e => reject_map(e));
	});
	return runNextMap();
}
// Exports depends of running context
// From CLI
if(require.main === module) {
	const cliParams = process.argv.slice(2);

	if(cliParams.length >= 2) {
		const mode = cliParams.shift();
		const themeId = cliParams.shift();

		if(mode === "init") {
			console.log("Start initialization for theme", themeId);
			return initTheme(themeId).catch(e => console.error(e));
		}
		else if(mode === "query") {
			console.log("Start query for theme", themeId, "with options", JSON.stringify(cliParams));
			return queryTheme(themeId, cliParams)
			.then(file => console.log("Output file:", file))
			.catch(e => console.error(e));
		}
		else {
			console.error("Invalid mode: "+mode+" (should be either init or query)");
		}
	}
	else {
		console.log("Usage: node runner.js (init|query) <THEME ID> [other options]");
	}
}
// As a module
else {
	exports.initTheme = initTheme;
	exports.queryTheme = queryTheme;
	exports.queryConverterTheme = queryConverterTheme;
	exports.hasDataExposerTheme = hasDataExposerTheme;
	exports.queryExposerTheme = queryExposerTheme;
}
