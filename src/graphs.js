const background_colors =  [
	'#e76f51',
	'#f4a261',
	'#e9c46a',
	'#2a9d8f',
	'#264653',
	'rgba(255, 159, 64, 0.2)'
];

function getGraphConfig(config, rows, boundary_name) {
	// Build the skeleton config for graphs
	const graph = {
		_show_percentages: (typeof variable !== 'config["show_percentages"]') ? config["show_percentages"] : true,
		type: config["subtype"],
		data: {},
		options: {
			layout: {
				padding: 30
			},
			scales: {
				xAxes: {},
				yAxes: {},
			},
			plugins: {
				legend: {
					display: Object.keys(config["datasets"]).length > 1,
				},
				title: {
					display: true,
					text: `${config["title"]} - ${boundary_name}`,
				},
			},
			responsive: true,
			maintainAspectRatio: false,
		},
	};

	setDatasets(config, graph);

	if (graph.type == "pie") {
		setPieLabels(config, graph, rows);

		graph.options.plugins.legend.display = true;
		graph.options.scales.xAxes["display"] = false;
		graph.options.scales.yAxes["display"] = false;
		return graph;
	}
	// Add the x axis title if present in the config
	if (config.x_axis_title) {
		graph["options"]["scales"]["xAxes"]["title"] = {
			display: true,
			text: config.x_axis_title,
			font: { size: 15 },
		};
	}
	// Add the y axis title if present in the config
	if (config.y_axis_title) {
		graph["options"]["scales"]["yAxes"]["title"] = {
			display: true,
			text: config.y_axis_title,
			font: { size: 15 },
		};
	}

	// Add annotations if present in the config
	if (config.annotations) {
		var annotations = {};
		config.annotations.forEach((a) => {
			annotations[a.label] = {
				type: a.type,
				borderColor: a.color,
				borderWidth: 2,
				label: {
					display: true,
					content: a.label,
					backgroundColor: '#adadad'
				},
				scaleID: config?.["direction"] == "horizontal" ? "xAxes" : "yAxes",
				value: a.value,
				endValue: a.value,
			};
		});
		graph.options.plugins["annotation"] = {
			annotations: annotations
		}
	}

	if (graph.type == "bar") {
		if (config?.["direction"] == "horizontal")
			// Invert the index axis
			graph.options["indexAxis"] = "y";
	} else if (graph.type == "line") {
		// Set the minimum x scale to 0
		graph["options"]["scales"]["yAxes"]["min"] = 0;
	}
	return graph;
}

function setDatasets(config, graph) {
	const datasets = [];
	var labelKey;
	var valueKey;
	if (graph.type == "pie") {
		labelKey = "id";
		valueKey = "value";
	} else {
		var annotationScale;
		if (config?.["direction"] == "horizontal") {
			labelKey = "yAxisKey";
			valueKey = "xAxisKey";
		} else {
			labelKey = "xAxisKey";
			valueKey = "yAxisKey";
		}

	}
	// Build the datasets
	// Note: datasets.data will be set on the client side,
	// _valueKey will be used on the client side to display the data values
	config["datasets"].forEach((ds) => {
		const dataset = {
			label: ds["label"],
			stack: ds.stack ? ds.stack : undefined,
			parsing: {
				_valueKey: valueKey,
				[labelKey]: ds["labelKey"],
				[valueKey]: ds["valueKey"],
			},
		};
		dataset["backgroundColor"] = ds["color"] ? ds["color"] : background_colors;
		datasets.push(dataset);
	});
	graph["data"]["datasets"] = datasets;
}

/**
 * Function to set the labels for a pie chart.
 * Because pie chart can't use the same parsing options as the other graphs type (bar, line),
 * we have to build the label list ourself. We do this row by row (having a small number of rows
 * returned by the sql query is important both for performance and visibility of the graph).
 * @param {json} config the theme config
 * @param {Object} graph the graph config
 * @param {Array} rows the rows of the sql query result
 */
function setPieLabels(config, graph, rows) {
	const labels = [];
	var colors = background_colors.slice(0);
	const used_colors = [];

	rows.forEach((row) => {
		var label = row[graph.data.datasets[0].parsing.id];
		if (config.labels[label]) {
			if (config.labels[label].color) used_colors.push(config.labels[label].color);
			else used_colors.push(colors.shift());
			label = config.labels[label].name;
		} else {
			// Capitalize label
			label = label.charAt(0).toUpperCase() + label.slice(1).toLowerCase();
			used_colors.push(colors.shift());
		}

		labels.push(label);
	});
	graph["data"]["labels"] = labels;
	// There should be only one dataset for a pie chart
	graph["data"]["datasets"][0]["backgroundColor"] = used_colors;
}

module.exports = { getGraphConfig };
