const { Client } = require('pg');
const { getDbClient } = require('./utils');
const fs = require('fs');
const dataForge = require('data-forge-fs');
const csvparse = require('csv-parse/sync');
const { delimiter } = require('path');
const geozones = require(__dirname+'/../assets/zones.json');


// Parsing file containing data about cities
const cog = csvparse.parse(fs.readFileSync(__dirname+'/../assets/communes.csv'), { columns: true });

// Parsing file containing data about postal code
const cp = csvparse.parse(fs.readFileSync(__dirname+'/../assets/laposte_hexasmal.csv'), { columns: true , delimiter: ';'});

// Group features
const communesCog = {};
cog.forEach(c => {
	if(!communesCog[c.COM] || c.TYPE === "COM") {
		communesCog[c.COM] = c;
	}
});

const communesCp =  {};
cp.forEach(c => {

	// Using insee code isn't enough to make it unique since there are cities with the same insee code but different postal code
	// The key is now the combination of them as a string
	if(!(c.Code_commune_INSEE.toString()+c.Code_postal.toString() in communesCp))  // Adding value only once
	{
		communesCp[c.Code_commune_INSEE.toString()+c.Code_postal.toString()] = c;
	}

});

const communes = {};
geozones.features.filter(f => f.properties.level === "fr:commune").forEach(f => communes[f.properties.code] = f);
const epcis = {};
geozones.features.filter(f => f.properties.level === "fr:epci" && f.properties.validity.end === null).forEach(f => epcis[f.properties.id] = f);
const departements = {};
geozones.features.filter(f => f.properties.level === "fr:departement" && f.properties.validity.end === null).forEach(f => departements[f.properties.code] = f);
const arrondissements = {};
geozones.features.filter(f => f.properties.level === "fr:arrondissement").forEach(f => arrondissements[f.properties.code] = f);
const regions = {};
geozones.features.filter(f => f.properties.level === "fr:region" && f.properties.validity.end === null).forEach(f => regions[f.properties.code] = f);


// Report found features
console.log("COG Commune properties", Object.values(communesCog)[0]);
console.log("Code postal properties:", Object.values(communesCp)[0]);
console.log("Commune properties", Object.values(communes)[0].properties);
console.log("EPCI properties", Object.values(epcis)[0].properties);
console.log("Département properties", Object.values(departements)[0].properties);
console.log("Arrondissement properties", Object.values(arrondissements)[0].properties);
console.log("Région properties", Object.values(regions)[0].properties);
console.log("=====================================");
console.log("Communes :", Object.keys(communes).length);
console.log("EPCIs :", Object.keys(epcis).length);
console.log("Départements :", Object.keys(departements).length);
console.log("Arrondissements :", Object.keys(arrondissements).length);
console.log("Régions :", Object.keys(regions).length);
console.log("Code postal :", Object.values(communesCp).length);

// Create database-ready rows
const communesDb = Object.values(communesCog).map(c => ([
	c.COM,
	c.LIBELLE,
	communes[c.COM] ? communes[c.COM].properties.parents.filter(p => p.startsWith("fr:epci") && epcis[p]).map(p => epcis[p].properties.code).pop() : null,
	communes[c.COM] ? communes[c.COM].properties.parents.filter(p => p.startsWith("fr:epci") && epcis[p]).map(p => epcis[p].properties.name).pop() : null,
	arrondissements[c.ARR] ? arrondissements[c.ARR].properties.code : null,
	arrondissements[c.ARR] ? arrondissements[c.ARR].properties.name : null,
	departements[c.DEP] ? departements[c.DEP].properties.code : null,
	departements[c.DEP] ? departements[c.DEP].properties.name : null,
	regions[c.REG] ? regions[c.REG].properties.code : null,
	regions[c.REG] ? regions[c.REG].properties.name : null,
	communes[c.COM]?.geometry ? JSON.stringify(communes[c.COM].geometry) : null
]));

const departementsDb = Object.values(departements).map(c => ([
	c.properties.code,
	c.properties.name,
	c.properties.parents.filter(p => p.startsWith("fr:region") && regions[p]).map(p => regions[p].properties.code).pop(), // Région ID
	c.properties.parents.filter(p => p.startsWith("fr:region") && regions[p]).map(p => regions[p].properties.name).pop(), // Région name
	JSON.stringify(c.geometry)
]));

const cpDb =  Object.values(communesCp).map(c => ([
	c.Code_commune_INSEE,
	c.Nom_commune,
	c.Code_postal
]));


// Import into DB
const db = getDbClient();

function appendInLot(tableName, tableStruct, data) {
	console.log("Start importing "+tableName);
	return db.query(`DROP TABLE IF EXISTS ${tableName} CASCADE; CREATE TABLE ${tableName}(${tableStruct})`).then(() => {
		let current = 0;
		let lotSize = 200;

		const nextInsert = () => {
			if(current < data.length) {
				const lot = data.slice(current, current + lotSize);
				process.stdout.clearLine();
				process.stdout.cursorTo(0);
				process.stdout.write(`Processing ${tableName} ${current} - ${current+lot.length} / ${data.length}`);
				let sql = `INSERT INTO ${tableName} VALUES `;
				for(let i=0; i < lot.length; i++) {
					sql += `${i > 0 ? ",": ""} (${lot[i].map((v,j) => `$${i * lot[0].length + j + 1}`)})`;
				}

				return db.query(sql, lot.flat()).then(() => {
					current += lotSize;
					return nextInsert();
				});
			}
			else {
				console.log("\nReindex");
				return db.query("REINDEX TABLE "+tableName).then(() => {
					console.log("Import done for "+tableName+"\n");
					return true;
				});
			}
		};

		return nextInsert();
	});
}


// Function to merge 2 csv file into another.
// Conditions : columnsFile1 = columnsFile2
function mergeCSV(filename1,  filename2, outputFilename) {
	console.log("Start merging CSV files");
	console.log("File 1 : "+filename1);
	console.log("File 2 : "+filename2);

	// Load CSV as dataframe
	var df1 = dataForge.readFileSync(__dirname+'/../assets/'+filename1).parseCSV();
	var df2 = dataForge.readFileSync(__dirname+'/../assets/'+filename2).parseCSV();

	df1 = df1
	.join(
		df2,
		(rowA) => rowA.COM,
		(rowB) => rowB.Code_commune_INSEE,
		(rowA, rowB) => {
			return {
				// New table after join
				TYPECOM : rowA.TYPECOM,
				COM: rowA.COM,
				cp: rowB.Code_postal,
				REG: rowA.REG,
				DEP: rowA.DEP,
				CTCD: rowA.CTCD,
				ARR: rowA.ARR,
				TNCC: rowA.TNCC,
				NCC: rowA.NCC,
				NCCENR: rowA.NCCENR,
				LIBELLE: rowA.LIBELLE,
				CAN: rowA.CAN,
				COMPARENT: rowA.COMPARENT
			};
		}
	);

	df1.asCSV().writeFileSync(__dirname+'/../assets/'+outputFilename);

	console.log("CSV files merged");
}
const sqlBoundaries = `
CREATE EXTENSION IF NOT EXISTS postgis;
CREATE EXTENSION IF NOT EXISTS unaccent;
CREATE EXTENSION IF NOT EXISTS pg_trgm;
DROP TABLE IF EXISTS boundaries;
CREATE TABLE boundaries AS
SELECT 'commune' AS type, code, name, lower(unaccent(name)) AS simple_name FROM communes
UNION ALL
SELECT 'epci' AS type, code, name, lower(unaccent(name)) AS simple_name FROM epcis
UNION ALL
SELECT 'arrondissement' AS type, code, name, lower(unaccent(name)) AS simple_name FROM arrondissements
UNION ALL
SELECT 'departement' AS type, code, name, lower(unaccent(name)) AS simple_name FROM departements
UNION ALL
SELECT 'region' AS type, code, name, lower(unaccent(name)) AS simple_name FROM regions;
CREATE INDEX boundaries_code_idx ON boundaries(code);
CREATE INDEX boundaries_simple_name_trgm_idx ON boundaries USING GIST(simple_name gist_trgm_ops);
ALTER TABLE regions ADD geo_center GEOMETRY(POINT, 4326);
ALTER TABLE arrondissements ADD geo_center GEOMETRY(POINT, 4326);
ALTER TABLE epcis ADD geo_center GEOMETRY(POINT, 4326);
ALTER TABLE departements ADD geo_center GEOMETRY(POINT, 4326);
ALTER TABLE communes ADD geo_center GEOMETRY(POINT, 4326);
UPDATE regions SET geo_center = ST_Centroid(geo_boundary);
UPDATE arrondissements SET geo_center = ST_Centroid(geo_boundary);
UPDATE epcis SET geo_center = ST_Centroid(geo_boundary);
UPDATE departements SET geo_center = ST_Centroid(geo_boundary);
UPDATE communes SET geo_center = ST_Centroid(geo_boundary);
`;

return db.connect()
	.then(() => appendInLot("regions", "code VARCHAR PRIMARY KEY, name VARCHAR, geo_boundary geometry(MULTIPOLYGON, 4326)", Object.values(regions).map(e => [e.properties.code, e.properties.name, JSON.stringify(e.geometry)])))
	.then(() => appendInLot("arrondissements", "code VARCHAR PRIMARY KEY, name VARCHAR, geo_boundary geometry(MULTIPOLYGON, 4326)", Object.values(arrondissements).map(e => [e.properties.code, e.properties.name, JSON.stringify(e.geometry)])))
	.then(() => appendInLot("epcis", "code VARCHAR PRIMARY KEY, name VARCHAR, geo_boundary geometry(MULTIPOLYGON, 4326)", Object.values(epcis).map(e => [e.properties.code, e.properties.name, JSON.stringify(e.geometry)])))
	.then(() => appendInLot("departements", "code VARCHAR PRIMARY KEY, name VARCHAR, region VARCHAR REFERENCES regions(code), region_name VARCHAR, geo_boundary geometry(MULTIPOLYGON, 4326)", departementsDb))
	.then(() => appendInLot("communes", "code VARCHAR PRIMARY KEY, name VARCHAR, epci VARCHAR REFERENCES epcis(code), epci_name VARCHAR, arrondissement VARCHAR REFERENCES arrondissements(code), arrondissement_name VARCHAR, departement VARCHAR REFERENCES departements(code), departement_name VARCHAR, region VARCHAR REFERENCES regions(code), region_name VARCHAR, geo_boundary geometry(MULTIPOLYGON, 4326)", communesDb))
	.then(() => appendInLot("communes_cp", "Code_commune_INSEE VARCHAR, Nom_commune VARCHAR, Code_postal VARCHAR, PRIMARY KEY(Code_commune_INSEE,Code_postal)", cpDb))
	.then(() => db.query(sqlBoundaries))
	.then(() => db.query("CREATE TABLE IF NOT EXISTS themes(id VARCHAR PRIMARY KEY, last_update TIMESTAMP)"))
	.then(() => db.end())
	.catch(e => { console.error(e); db.end(); });
