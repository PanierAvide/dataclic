const http = require('http');
const https = require('https');
const fs = require('fs');
const readline = require('readline');
const FILE_CONFIG = require('../config.json');
const { Client } = require('pg');

const CONFIG = {
	DB_HOST: process.env.DB_HOST || FILE_CONFIG.DB_HOST,
	DB_NAME: process.env.DB_NAME || FILE_CONFIG.DB_NAME,
	DB_PORT: process.env.DB_PORT || FILE_CONFIG.DB_PORT,
	DB_USER: process.env.DB_USER || FILE_CONFIG.DB_USER,
	DB_PASSWORD: process.env.DB_PASSWORD || FILE_CONFIG.DB_PASSWORD,
	DATA_FOLDER: process.env.DATA_FOLDER || FILE_CONFIG.DATA_FOLDER,
	BASE_URL: process.env.BASE_URL || FILE_CONFIG.BASE_URL,
	MAPTILER_API_KEY: process.env.MAPTILER_API_KEY || FILE_CONFIG.MAPTILER_API_KEY,
	TILESERV_URL: process.env.TILESERV_URL || FILE_CONFIG.TILESERV_URL
};
exports.CONFIG = CONFIG;
exports.ADMIN_TYPES = ["commune", "epci", "arrondissement", "departement", "region"];

/**
 * Creates a client for database. It should be connected manually.
 */
exports.getDbClient = () => {
	return new Client({
		host: CONFIG.DB_HOST,
		database: CONFIG.DB_NAME,
		port: CONFIG.DB_PORT,
		user: CONFIG.DB_USER,
		password: CONFIG.DB_PASSWORD,
	});
};

/**
 * Get file name in an URL
 * @param {string} url The input URL
 * @return {string} The file name
 */
exports.getUrlFileName = (url) => {
	return url.split('/').pop().split('#')[0].split('?')[0];
};

/**
 * Get a file name without its extension
 * @param {string} filename The file name
 * @return {string} The base name without extension
 */
exports.getBasename = filename => {
	return filename.substr(0, filename.lastIndexOf('.'));
};

/**
 * Downloads file from URL
 * @param {string} url The URL to download from
 * @param {string} path The location to write file on disk
 * @param {boolean} [logProgress] True for display download progress in console (defaults to false)
 * @return {Promise} Resolves on success
 */
function download(url, path, logProgress = false) {
	return new Promise((resolve, reject) => {
		if(logProgress) { process.stdout.write("Start download"); }
		let handler = http;

		if(/^https:\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?$/.test(url)) {
			handler = https;
		}

		handler.get(url, (res) => {
			const length = parseInt(res.headers['content-length'], 10);
			let currentLength = 0;
			const total = isNaN(length) ? null : length / 1048576; // Nb of bytes in a Mb

			switch(res.statusCode) {
				case 200:
					const file = fs.createWriteStream(path);
					res.pipe(file);

					file.on('finish', () => {
						if(logProgress) { process.stdout.write('\n'); }
						file.close(resolve);
					});

					if(logProgress) {
						res.on('data', chunk => {
							currentLength += chunk.length;
							readline.clearLine(process.stdout, 0);
							readline.cursorTo(process.stdout, 0);
							if(total) {
								process.stdout.write(`Downloading ${(100.0 * currentLength / length).toFixed(2)}% | ${(currentLength / 1048576).toFixed(2)} / ${total.toFixed(2)} Mb`);
							}
							else {
								process.stdout.write(`Downloading ${(currentLength / 1048576).toFixed(2)} Mb`);
							}
						});
					}
					break;
				case 301:
				case 302:
				case 303:
				case 307:
					resolve(download(res.headers.location, path, logProgress));
					break;
				default:
					reject(`Can't download file from ${url} (HTTP error ${res.statusCode})`);
			}

		})
		.on('error', err => reject(err));
	});
}
exports.download = download;
