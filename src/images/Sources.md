# Images sources

Some pictures used in this website are made by third-party under an open license.

* `desert.jpg` : AndrewKPepper under CC-By-SA ( via https://commons.wikimedia.org/wiki/File:Death_Valley_02.jpg )
* `bigdata.jpg` : Pixabay under CC0 ( via https://commons.wikimedia.org/wiki/File:Art-big-data-blur-373543.jpg )
