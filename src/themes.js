const fs = require('fs');
const marked = require('marked');
const THEMES_PATH = __dirname + '/../themes';

const themes = {};
fs.readdirSync(THEMES_PATH).forEach(themeDir => {
	try {
		const theme = JSON.parse(fs.readFileSync(THEMES_PATH + '/' + themeDir + '/info.json'));
		theme.id = themeDir;

		// Try to read Markdown info file, if exists
		try {
			theme.info = marked(fs.readFileSync(THEMES_PATH + '/' + themeDir + '/info.md', "utf8"));
		} catch(e) {
			theme.info = "";
		}

		themes[themeDir] = theme;
	}
	catch(e) {
		console.error("Invalid theme", themeDir, e);
	}
});

module.exports = themes;
