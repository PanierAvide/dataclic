/**
 * API main code
 */

// Imports
const express = require('express');
const cors = require('cors');
const compression = require('compression');
const fileUpload = require('express-fileupload');
const path = require('path');
const fs = require('fs');
const themes = require('./themes');
const { getDbClient, download, getBasename, getUrlFileName, ADMIN_TYPES, CONFIG } = require('./utils');
const { queryTheme, queryConverterTheme, queryExposerTheme, hasDataExposerTheme } = require('./runner');
const crypto = require("crypto");
const { getGraphConfig } = require('./graphs');


// Init API
const app = express();
const port = process.env.PORT || 3000;
app.use(cors());
app.options('*', cors());

app.use(compression());
app.use(fileUpload({
	limits: { fileSize: 50 * 1024 * 1024 },
}));
app.set('view engine', 'pug');
app.set('views', __dirname+'/templates');


// Main page
app.get('/', (req, res) => {
	res.render('pages/index', { themes });
});

// Images
app.use('/images', express.static(__dirname+'/images'));


// HTTP errors
app.get('/error/:code', (req, res) => {
	const httpcode = req.params.code && !isNaN(req.params.code) ? req.params.code : "400";
	res.status(httpcode).render('pages/error', { themes, httpcode });
});


// Theme page
app.get('/:themeId', (req, res) => {
	if(!themes[req.params.themeId]) {
		return res.redirect('/error/404');
	}

	const theme = themes[req.params.themeId];

	const db = getDbClient();
	db.connect()
	.then(() => db.query("SELECT * FROM themes WHERE id = $1", [theme.id]))
	.then(result => {
		res.render(`pages/theme_${theme.type}`, { theme, themes, themeMeta: result.rows.pop() });
	})
	.catch(e => {
		console.error(e);
		res.status(500).send("Can't connect to database (it's our fault, not yours)");
	})
	.finally(() => db.end());
});


/**
 * @api {get} /api/doc API documentation
 * @apiDescription Get the user documentation of the API (this page)
 * @apiName GetDoc
 * @apiGroup Default
 */
app.use('/api/doc', express.static(__dirname+'/../doc'));


/**
 * @api {get} /api/boundaries Search administrative boundaries
 * @apiDescription Look for specific administrative boundaries searching by their name or official reference.
 * @apiName GetBoundaries
 * @apiGroup Default
 *
 * @apiParam {String} text The text search pattern
 *
 * @apiExample Search boundaries with name like "acigne"
 *     https://dataclic.fr/api/boundaries?text=acigne
 *
 * @apiSuccessExample {json} Response
 *   HTTP/1.1 200 OK
 *   [
 *     {
 *       "type": "commune",
 *       "code": "35001",
 *       "name": "Acigné"
 *     },
 *     ...
 *   ]
 */
app.get("/api/boundaries", (req, res) => {
	const text = req.query.text.trim() || "";
	if(text.length === 0) {
		return res.send([]);
	}

	const db = getDbClient();
	let search;

	// Search by ref
	if(text.match(/^[0-9]+$/)) {
		search = () => db.query(`SELECT type, code, name FROM boundaries WHERE code LIKE '${text}%' ORDER BY code LIMIT 10`)
		.then(res => res.rows);
	}
	// Search by name
	else {
		search = () => db.query(`SELECT type, code, name FROM boundaries ORDER BY lower(unaccent($1)) <-> simple_name LIMIT 10`, [ text ])
		.then(res => res.rows);
	}

	db.connect()
	.then(search)
	.then(b => {
		return res.send(b);
	})
	.catch(e => {
		console.error(e);
		res.status(500).send("Can't search in boundaries: " + e.message);
	})
	.finally(() => db.end());
});


/**
 * @api {get} /api/themes Available themes
 * @apiDescription Get list of all available themes and their details
 * @apiName GetThemes
 * @apiGroup Default
 *
 * @apiSuccessExample {json} Response
 *   HTTP/1.1 200 OK
 *   {
 *     "budget": {
 *       "id": "budget",
 *       "name": "Budget",
 *       "type": "converter",
 *       "parameters": {
 *         "file": {
 *           "description": "Budget XML file",
 *            "type": "file (text/xml)"
 *         }
 *       }
 *     },
 *     "entreprises": {
 *       "id": "entreprises",
 *       "name": "Entreprises",
 *       "type": "exposer"
 *     }
 *   }
 */
app.get("/api/themes", (req, res) => {
	const cleanedUpThemes = {};

	Object.values(themes).forEach(t => {
		cleanedUpThemes[t.id] = {
			id: t.id,
			name: t.name,
			type: t.type
		};

		if(t.parameters) {
			const params = {};
			t.parameters.forEach(p => {
				params[p.id] = {
					description: p.name,
					type: p.type === "file" ? "file ("+p.format+")" : p.type
				};
			});
			cleanedUpThemes[t.id].parameters = params;
		}
	});

	res.send(cleanedUpThemes);
});


// Object containing running tasks
const tasks = {};
const originalFileNames = {};

function updateProgress(hash) {
	return (status, val) => {
		tasks[hash].status = status;
		if(status === "running") { tasks[hash].progress = val; }
		else if(status === "error") {
			delete tasks[hash].progress;
			tasks[hash].reason = val;
		}
		else if(status === "ready") {
			delete tasks[hash].progress;
			tasks[hash].file = val;
		}
	};
}

/**
 * Handle processing of themes queries after file is retrieved
 * (either exists locally, is downloaded or posted by user)
 */
function queryThemePostFileProcess(req, res, theme, options, hash) {
	// Launch query
	return queryTheme(req.params.themeId, options)
	.then(filename => new Promise((resolve, reject) => {
		// Read file and send it
		const stat = fs.statSync(filename);
		const day = (new Date()).toISOString().split("T")[0];

		let outfile;
		if(theme.type === "exposer") { outfile = `${theme.id}_${options.slice(0,2).join("")}_${day}.csv`; }
		else if(theme.type === "converter") { outfile = `${originalFileNames[hash]}.${theme.output.extension}`; }

		res.writeHead(200, {
			'Content-Type': theme.type === "exposer" ? "text/csv" : theme.output.format,
			'Content-Length': stat.size,
			'Content-Disposition': `attachment; filename="${outfile}"`
		});

		const readStream = fs.createReadStream(filename);
		readStream.pipe(res);

		// Handle errors on sending
		readStream.on('error', (err) => {
			readStream.end();
			reject(err);
		});

		// Delete output file
		readStream.on('end', () => {
			fs.unlink(filename, (err) => {
				if(err) {
					console.error("Can't delete tmp file: ", err);
				}
				resolve();
			});
		});
	}))
	.catch(e => {
		console.error(e);
		res.status(500).send(e);
	});
}

/*
 * Generate unique hash for tracking request progress
 * @param {string} Some request-related string (for example a filename or URL)
 * @return {string} Assigned hash
 */
function getUniqueProgressHash(str) {
	let ok = false;
	let hash = "";
	let suffix = "";
	while(!ok) {
		hash = crypto.createHmac("md5", Date.now().toString()).update(str+suffix).digest("hex");
		if(!tasks[hash]) {
			ok = true;
			tasks[hash] = { status: "running", progress: 0 };
		}
		else {
			suffix += "a";
		}
	}
	return hash;
}


/** @apiDefine exposer Exposer-specific parameters */
/** @apiDefine th_budget Budget-specific parameters */
/**
 * @api {get} /api/themes/:themeId Thematic data extract
 * @apiDescription Get data for given theme as a file
 * @apiName GetTheme
 * @apiGroup Default
 *
 * @apiParam {String} themeId Theme of data to extract (one in /api/themes list)
 * @apiParam {boolean} [async] Launch request as asynchronous, receive a progress ID instead of result file (defaults to false)
 *
 * @apiParam (exposer) {String=commune,epci,arrondissement,departement,region} admtype The type of administrative boundary
 * @apiParam (exposer) {String} admcode The administrative boundary reference
 *
 * @apiParam (th_budget) {String} file URL to access a Totem XML budget file
 *
 * @apiExample Companies in Acigné (city with code 35001)
 *     https://dataclic.fr/api/themes/entreprises?admtype=commune&admcode=35001
 * @apiExample Converted and anonymized budget (from a web source)
 *     https://dataclic.fr/api/themes/budget?file=https%3A%2F%2Fdatacat.datalocale.fr%2Fdistribution%2F4348973%2Fraw%2FCA2019BPAL.XML
 */
app.get("/api/themes/:themeId", (req, res) => {
	// Check if theme exists
	if(!themes[req.params.themeId]) {
		return res.status(404).send("No theme with ID: "+req.params.themeId);
	}

	const theme = themes[req.params.themeId];

	// Check if mandatory parameters are set
	if(theme.type === "exposer" && (!req.query.admtype || !req.query.admcode)) {
		return res.status(400).send("Theme "+req.params.themeId+" needs parameters admtype and admcode");
	}
	else if(theme.type === "converter") {
		const missingParams = theme.parameters.filter(p => !req.query[p.id]).map(p => `${p.id} (${p.type})`);
		if(missingParams.length > 0) {
			return res.status(400).send("Theme "+req.params.themeId+" needs following parameters: "+missingParams.join(", "));
		}
	}

	// Process exposer theme
	if(theme.type === "exposer") {
		// Send data as stream
		res.writeHead(200, {
			'Content-Type': "text/csv",
			'Content-Disposition': `attachment; filename="${theme.id}_${req.query.admtype}${req.query.admcode}_${(new Date()).toISOString().split("T")[0]}.csv"`
		});

		queryExposerTheme(theme, [ req.query.admtype, req.query.admcode ], res);
	}
	else if(theme.type === "converter") {
		// File param name
		let fileParam = theme.parameters.find(p => p.type === "file");
		if(!fileParam) { return res.status(500).send("Theme "+req.params.themeId+" miss a file parameter"); }

		// Download file
		const url = req.query[fileParam.id];
		const hash = getUniqueProgressHash(url);
		originalFileNames[hash] = getBasename(getUrlFileName(url));
		const file = `${CONFIG.DATA_FOLDER}/${theme.id}_${hash}`;
		download(url, file)
		.then(() => {
			const options = theme.parameters.map(p => p.type === "file" ? file : req.query[p.id]);
			if(req.query.async === "true") {
				queryConverterTheme(theme, options, updateProgress(hash))
				.then(() => fs.unlink(file, (err) => {;}));
				return res.send(hash);
			}
			else {
				return queryThemePostFileProcess(req, res, theme, options, hash)
				.then(() => fs.unlink(file, (err) => {;}));
			}
		})
		.catch(e => {
			fs.unlink(file, (err) => {;});
			console.error(e);
			res.status(500).send(e);
		});
	}
});


/**
 * @api {get} /api/themes/:themeId/checkdata Check data availability
 * @apiDescription Check if data is available in a given territory
 * @apiName GetCheckData
 * @apiGroup Default
 *
 * @apiParam {String} themeId Theme of data to check (one in /api/themes list)
 * @apiParam {String=commune,epci,arrondissement,departement,region} admtype The type of administrative boundary
 * @apiParam {String} admcode The administrative boundary reference
 *
 * @apiExample Availability of companies in Acigné (city with code 35001)
 *     https://dataclic.fr/api/themes/entreprises/checkdata?admtype=commune&admcode=35001
 * @apiSuccessExample {text} Response
 *   HTTP/1.1 200 OK
 *   true
 */
app.get("/api/themes/:themeId/checkdata", (req, res) => {
	// Check if theme exists
	if(!themes[req.params.themeId]) {
		return res.status(404).send("No theme with ID: "+req.params.themeId);
	}

	const theme = themes[req.params.themeId];

	// Check if mandatory parameters are set
	if(theme.type === "exposer" && (!req.query.admtype || !req.query.admcode)) {
		return res.status(400).send("Theme "+req.params.themeId+" needs parameters admtype and admcode");
	}

	// Process exposer theme
	if(theme.type === "exposer") {
		// Return true if there is data in the DB
		hasDataExposerTheme(theme, [ req.query.admtype, req.query.admcode ])
		.then(hasData => res.send(hasData))
		.catch(e => {
			console.error(e);
			res.status(500).send(e);
		});
	}
});


/**
 * @api {post} /api/themes/:themeId File converter
 * @apiDescription Convert a file from your computer into a standardized dataset
 * @apiName PostTheme
 * @apiGroup Default
 *
 * @apiParam {String} themeId Converter theme ID (one in /api/themes list)
 * @apiParam {boolean} [async] Launch request as asynchronous, receive a progress ID instead of result file (defaults to false)
 *
 * @apiParam (th_budget) {File} file Totem XML budget file
 */
app.post("/api/themes/:themeId", (req, res) => {
	// Check if theme exists
	if(!themes[req.params.themeId]) {
		return res.status(404).send("No theme with ID: "+req.params.themeId);
	}

	const theme = themes[req.params.themeId];

	// Check if theme is converter
	if(theme.type !== "converter") {
		return res.status(405).send("Theme "+theme.id+" is not a converter, please use GET /api/themes/"+theme.id+" instead.");
	}

	// File param name
	let fileParam = theme.parameters.find(p => p.type === "file");
	if(!fileParam) { return res.status(500).send("Theme "+req.params.themeId+" miss a file parameter"); }

	// Check if a file was sent
	else if(!req.files || Object.keys(req.files).length === 0 || !req.files[fileParam.id]) {
		return res.status(400).send("No input file provided");
	}
	// Check file format
	else if(req.files[fileParam.id].mimetype !== fileParam.format) {
		return res.status(400).send("Sent file is not in expected format, expected "+fileParam.format+", received "+req.files[fileParam.id].mimetype);
	}

	// Move file to destination path
	const file = req.files[fileParam.id];
	const hash = getUniqueProgressHash(file.name);
	originalFileNames[hash] = getBasename(file.name);
	const filename = `${CONFIG.DATA_FOLDER}/${theme.id}_${hash}`;
	file.mv(filename, err => {
		if(err) {
			console.error(err);
			return res.status(500).send(err);
		}

		const options = theme.parameters.map(p => p.type === "file" ? filename : req.query[p.id]);
		if(req.query.async === "true") {
			queryConverterTheme(theme, options, updateProgress(hash))
			.then(() => fs.unlink(filename, (err) => {;}));
			return res.send(hash);
		}
		else {
			return queryThemePostFileProcess(req, res, theme, options, hash)
			.then(() => fs.unlink(filename, (err) => {;}));
		}
	});
});


/**
 * @api {get} /api/themes/:themeId/metadata Metadata for data extract
 * @apiDescription Get metadata as a CSV file for a data extract (exposer themes only)
 * @apiName GetThemeMetadata
 * @apiGroup Default
 *
 * @apiParam {String} themeId Theme of data to extract (one in /api/themes list)
 * @apiParam {String=commune,epci,arrondissement,departement,region} admtype The type of administrative boundary
 * @apiParam {String} admcode The administrative boundary reference
 *
 * @apiExample Metadata for companies in Acigné (city with code 35001)
 *     https://dataclic.fr/api/themes/entreprises/metadata?admtype=commune&admcode=35001
 */
app.get("/api/themes/:themeId/metadata", (req, res) => {
	// Check if theme exists
	if(!themes[req.params.themeId]) {
		return res.status(404).send("No theme with ID: "+req.params.themeId);
	}

	const theme = themes[req.params.themeId];

	// Check if theme is exposer
	if(theme.type !== "exposer") {
		return res.status(405).send("Metadata files are only available for exposer themes");
	}

	// Check input
	if(!req.query.admtype || !ADMIN_TYPES.includes(req.query.admtype)) {
		return res.status(404).send("Parameter admtype is missing or invalid, should be one of "+ADMIN_TYPES.join(", "));
	}
	if(!req.query.admcode || !/^[a-zA-Z0-9]+$/.test(req.query.admcode)) {
		return res.status(404).send("Parameter admcode is missing or invalid");
	}

	const db = getDbClient();
	db.connect()
	.then(() => db.query(`SELECT name FROM ${req.query.admtype}s WHERE code = $1`, [req.query.admcode]))
	.then(result => {
		if(result.rows.length === 1) {
			const admname = result.rows[0].name;

			return db.query("SELECT last_update FROM themes WHERE id = $1", [theme.id])
			.then(result2 => {
				const lastUpdate = result2.rows.length > 0 ? new Date(result2.rows[0].last_update).toISOString().split("T")[0] : "";

				const metadataTxt = `COLL_NOM,COLL_SIRET,ID,TITRE,DESCRIPTION,THEME,PRODUCTEUR_NOM,PRODUCTEUR_SIRET,COUV_SPAT_MAILLE,COUV_SPAT_NOM,COUV_TEMP_DEBUT,COUV_TEMP_FIN,DATE_PUBL,FREQ_MAJ,DATE_MAJ,MOTS_CLES,LICENCE,NOMBRE_RESSOURCES,FORMAT_RESSOURCES,URL
	${admname},,${theme.id}_${req.query.admtype}${req.query.admcode},${theme.name} de ${admname},${theme.description},${theme.metadata.category},${theme.metadata.producer},,${theme.metadata.precision},${admname},,,,${theme.metadata.updates},${lastUpdate},,${theme.metadata.licence},1,csv,${CONFIG.BASE_URL}/api/themes/${theme.id}?admtype=${req.query.admtype}&admcode=${req.query.admcode}`;

				res.set({
					'Content-Type': "text/csv",
					'Content-Disposition': `attachment; filename="${theme.id}_${req.query.admtype}${req.query.admcode}_metadata.csv"`
				});
				return res.send(metadataTxt);
			});
		}
		else {
			return res.status(404).send("Can't find given administrative boundary, please check admtype and admcode parameters");
		}
	})
	.catch(e => {
		console.error(e);
		res.status(500).send("Can't connect to database (it's our fault, not yours)");
	})
	.finally(() => db.end());
});


/**
 * @api {get} /api/tasks/:taskId Task progress
 * @apiDescription Get progress for a generated task
 * @apiName GetTask
 * @apiGroup Default
 *
 * @apiParam {String} taskId The task ID (always given by another API call)
 *
 * @apiSuccessExample {json} Running task
 *   HTTP/1.1 200 OK
 *   { "status": "running", "progress": 5 }
 *
 * @apiSuccessExample {json} Finished task
 *   HTTP/1.1 200 OK
 *   { "status": "ready" }
 *
 * @apiSuccessExample {json} Failed task
 *   HTTP/1.1 200 OK
 *   { "status": "error", "reason": "Something went wrong" }
 */
app.get("/api/tasks/:taskId", (req, res) => {
	if(!tasks[req.params.taskId]) {
		return res.status(404).send("Task ID is invalid");
	}

	const task = tasks[req.params.taskId];
	if(task.status === "ready") {
		return res.json({ status: "ready" });
	}
	else {
		return res.json(task);
	}
});


/**
 * @api {get} /api/tasks/:taskId/result Task result
 * @apiDescription Get result of a task (file). Should be called only if task status is "ready".
 * @apiName GetTaskResult
 * @apiGroup Default
 *
 * @apiParam {String} taskId The task ID (always given by another API call)
 */
app.get("/api/tasks/:taskId/result", (req, res) => {
	if(!tasks[req.params.taskId]) {
		return res.status(404).send("Task ID is invalid");
	}

	const task = tasks[req.params.taskId];

	if(task.status !== "ready") {
		return res.status(410).send("Result is either not ready yet or was in error");
	}

	// Send result file
	const stat = fs.statSync(task.file.servername);

	res.writeHead(200, {
		'Content-Type': task.file.mime,
		'Content-Length': stat.size,
		'Content-Disposition': `attachment; filename="${originalFileNames[req.params.taskId]}.${task.file.extension}"`
	});

	const readStream = fs.createReadStream(task.file.servername);
	readStream.pipe(res);

	// Handle errors on sending
	readStream.on('error', (err) => {
		readStream.end();
		res.status(500).send(err);
	});

	// Delete output file
	readStream.on('end', () => {
		fs.unlink(task.file.servername, (err) => {
			if(err) { console.error("Can't delete tmp file: ", err); }
		});
		delete tasks[req.params.taskId];
	});
});


/**
 * @api {get} /api/themes/:themeId/dataviz/:viz_id An Iframe containing a dataviz
 * @apiDescription Get an iframe with a graphical representation of the data.
 * @apiName GetDatavizIframe
 * @apiGroup Default
 *
 * @apiParam {String} themeId Theme of data to extract (one in /api/themes list)
 * @apiParam {String} viz_id The id of the dataviz
 * @apiParam {String=commune,epci,arrondissement,departement,region} admtype The type of administrative boundary
 * @apiParam {String} admcode The administrative boundary reference
 * @apiParam {String} boundary_name The name of the administrative boundary name 
 */
app.get('/api/themes/:themeId/dataviz/:viz_id', (req, res) => {
	// Check if theme exists
	if (!themes[req.params.themeId])
		return res.status(404).send("No theme with ID: " + req.params.themeId);

	const theme = themes[req.params.themeId];

	if (theme.type !== "exposer")
		return res.status(405).send("Dataviz is only available for exposer themes");

	if (!theme.dataviz)
		return res.status(404).send("This theme has no available dataviz");

	if (!theme.dataviz[req.params.viz_id])
		return res.status(404).send("Graph ID is invalid");

	// Check input
	if (req.query.admtype && !ADMIN_TYPES.includes(req.query.admtype))
		return res.status(404).send("Parameter admtype is missing or invalid, should be one of " + ADMIN_TYPES.join(", "));

	if (req.query.admcode && !/^[a-zA-Z0-9]+$/.test(req.query.admcode))
		return res.status(404).send("Parameter admcode is missing or invalid");


	if (theme.dataviz[req.params.viz_id].type == "map") {
		const api_key = CONFIG.MAPTILER_API_KEY;
		const tileserv_url = CONFIG.TILESERV_URL;
		const map_id = req.params.viz_id;
		const map_config = theme.dataviz[map_id];
		var bounds = [];

		const admtype = req.query.admtype;
		const admcode = req.query.admcode;
		if (!admtype || !admcode)
			return res.render(`pages/map-viz`, { api_key, tileserv_url, map_id, map_config, bounds, admtype, admcode });

		const query_sql = `
			SELECT ST_Extent(geo_boundary) as bextent
			FROM ${req.query.admtype}s
			WHERE code = '${req.query.admcode}';
		`;
		const db = getDbClient();
		db.connect()
			.then(() => db.query(query_sql, []))
			.then(result => {
				const row = result.rows[0];
				if (row.bextent) {
					const regex = /[+-]?([0-9]*[.])?[0-9]+/g;
					const [xmin, ymin, xmax, ymax] = row.bextent.match(regex);
					bounds = [[xmin, ymin], [xmax, ymax]];
				}

				return res.render(`pages/map-viz`, { api_key, tileserv_url, map_id, map_config, bounds, admtype, admcode });
			}).catch(e => {
				console.error(e);
				return res.status(500).send("Can't connect to database (it's our fault, not yours)");
			}).finally(() => db.end());
	} else {
		var admtype = "x_region_code";
		var admcode = "NULL";
		var boundary_name = "échelle nationale";

		if (req.query.admtype)
			admtype = `x_${req.query.admtype}_code`;
		if (req.query.admcode)
			admcode = `'${req.query.admcode}'`;
		if (req.query.boundary_name)
			boundary_name = req.query.boundary_name;
		
		const graph_id = req.params.viz_id;
		const graph = theme.dataviz[graph_id];
		var query_sql = fs.readFileSync(`./themes/${theme.id}/${graph.query_file}`, "utf-8");
		const regexes = {'<ADMTYPE>': admtype, '<ADMCODE>': admcode, ...graph.query_constants};

		Object.keys(regexes).forEach(r => {
			const regex = RegExp(r, 'g');
			query_sql = query_sql.replace(regex, regexes[r]);
		});

		const db = getDbClient();
		db.connect()
			.then(() => db.query(query_sql, []))
			.then(result => {
				const rows = result.rows;
				const graph_config = getGraphConfig(graph, rows, boundary_name);
				
				return res.render(`pages/graph-viz`, { graph_config, rows, theme });
			}).catch(e => {
				console.error(e);
				return res.status(500).send("Can't connect to database (it's our fault, not yours)");
			}).finally(() => db.end());
	}
});


const authorized = {
	"mdb-ui-kit": {
		"mdb.js": "js/mdb.min.js",
		"mdb.css": "css/mdb.min.css"
	},
	"chart.js": {
		"chart.js": "dist/chart.min.js"
	},
	"chartjs-plugin-datalabels": {
		"chartjs-plugin-datalabels.js": "dist/chartjs-plugin-datalabels.min.js"
	},
	"chartjs-plugin-annotation": {
		"chartjs-plugin-annotation.js": "dist/chartjs-plugin-annotation.min.js"
	},
	"maplibre-gl": {
		"maplibre-gl.js": "dist/maplibre-gl.js",
		"maplibre-gl.css": "dist/maplibre-gl.css"
	}
};

app.get('/lib/:modname/:file', (req, res) => {
	if(!req.params.modname || !req.params.file) {
		return res.status(400).send('Missing parameters');
	}
	else if(!authorized[req.params.modname] || !authorized[req.params.modname][req.params.file]) {
		return res.status(404).send('File not found');
	}

	const options = {
		root: path.join(__dirname, '../node_modules'),
		dotfiles: 'deny',
		headers: {
			'x-timestamp': Date.now(),
			'x-sent': true
		}
	};

	const fileName = `${req.params.modname}/${authorized[req.params.modname][req.params.file]}`;
	return res.sendFile(fileName, options, (err) => { if(err) { console.error(err, fileName); } });
});

app.use('/lib/fontawesome', express.static(path.join(__dirname, '../node_modules/@fortawesome/fontawesome-free')));

// No route found = 404
app.use((req, res) => {
	res.redirect('/error/404');
});


// Start
const tmpDb = getDbClient();
tmpDb.connect()
.then(() => tmpDb.query('SELECT version()'))
.then(() => {
	const server = app.listen(port, () => {
		server.keepAliveTimeout = 60 * 1000;
		server.headersTimeout = 65 * 1000;
		console.log('API started on port: ' + port);
	});
})
.catch(e => {
	console.error("Can't connect to database :", e.message);
})
.finally(() => tmpDb.end());
