# Dataviz
Exposer themes can be used to display two types of dataviz: graphs and maps, which will be displayed in an iframe below the boundary search bar. The dataviz are also accessible through the API to be embedded in other websites. The configuration file of the theme should have a property named dataviz containing:

* `name_of_graph`: the name of the dataviz, this name will appear in the url of the graph, special characters should therefore be avoided.
  * `type`: `graph` or `map`, depending on the dataviz you want to display
  * `subtype`: depends of `type` of dataviz:
    * `graph` type: `bar`, `line` or `pie`
    * `map` type: `marker`, `cluster` or `choropleth`
  * The additional properties of each type and subtype are explained below

## Graphs
* `query_file`: A file containing an sql query. The selected administrative boundary and code can be used in the query file with `<ADMIN_TYPE>` and `<ADMIN_CODE>`. Your query should return values for the whole table when no administrative boundary is selected, so it is very likely that you should use `(<ADMTYPE> = <ADMCODE> or <ADMCODE> is null)` as a condition in it.
* `query_constants`: This allows you to define constants that will be replaced before the query is executed. It should contain the following property for each constant:
  * `<PLACEHOLDER>`: The text to replace in the query
    * `MY_CONSTANT`: The constant value
* `title`: The title of the graph. Note: the selected administrative boundary will be added at the end of it.
* `description`: A short explanation of the graph, it can contain html tags for links, line breaks, etc...
* `direction`: Only for `bar` subtype, which can have the value `vertical` (default value if unspecified) or `horizontal`. This defines the direction of the bars.
* `show_percentages`: Optional, a boolean to decide whether percentages should be displayed on the graph, defaults to `true`.
* `x_axis_title`: Only for `bar` and `line` subtypes, a string containing the x axis title.
* `y_axis_title`: Only for `bar` and `line` subtypes, a string containing the y axis title.
* `datasets`: An array containing objects with the following properties:
  * `label`: The name of this dataset, it will be displayed only if there are multiple datasets.
  * `labelKey`: Each row returned by the sql query that contain this label will be considered as part of this dataset, this row should contain strings. Note: if your query returns a single row, where the values to plot are columns, add an empty string in column to your query results and use it's column name as `labelKey`.
  * `valueKey`: The column name of the value that will be plotted on the graph.
  * `color`: Optional, an array of colors that will be used for the points and legend of this dataset.
  * `stack`: Optional, only for `bar` subtype, an arbitrary string that will be used to stack bars on top of each other, from different datasets.
* `annotations`: Optional, only for `bar` and `line` subtypes, a list of objects to annotate your graph. The ojects should contain the following subproperties.
  * `type`: For now only `line` is supported. This will create a straight line.
  * `label`: A string that explains what the annotation represent.
  * `value`: A number representing the constant value of the annotation.
  * `color`: The color of the annotation.

Note : when using multiple datasets, it is very likely that they should all have the same `labelKey` because this value will be used as one of the axis.

### Pie chart specific property
When using a pie chart, there should only be one dataset. The `labelKey` has to point to a column which contains the labels. Because of this limitation, you can use this additional property to override the labels of the dataset:
* `labels`: An object containing one or more of the following object:
  * `VALUE_OF_THE_COLUMN`: A string containing a value that is expected in the `labelKey` column.
    * `name`: The name that will be used for this data.
    * `color`: Optional, a string containing the color code for this data.


## Maps
In order to create a map view, your table (or your view) has to contain a column named geom of type geometry with a SRID of 4326.
Note: for maps of type `choropleth` and `cluster`, views for each administrative boundaries will be created automatically when initializing the theme.

* `table`: The table / view name
* `marker`: Only for `choropleth` and `marker`, used to define the properties of a marker, it should be an object having the following properties:
  * `file`: The filename of the svg that will be used to mark individual items. This has to be selected in the [Font Awesome library](https://fontawesome.com/v5/search?m=free), and noted as `marker-name.svg` (by default `map-marker-alt.svg`)
  * `height`: Optional, the desired height of the svg (the aspect ratio of the original image will be kept), defaults to 20.
  * `color`: A string or an object representing the color of the marker, defaults to black. Using an object allows you to set different colors for the svg, depending on the value of one column. When using an object it should have the following sub-properties:
    * `column`: The column containing the value that should be compared
    * `case`: an object one or more of the following properties:
      * `VALUE_OF_THE_COLUMN`: A string containing a value. If a row contains this value (in the specified column), it will use the color specified in the following object:
        * `color`: The color that will be used of the marker.
        * `label`: A string that will be displayed in the legend, alongside the chosen color.
* `color_gradients`: Only for `choropleth` and `cluster` maps, an array containing pairs: `PERCENTAGE`, `COLOR`. This will be used to create a linear gradient, for example: `[0, "blue", 100, "red"]`: administrative boundaries with few points will have a blue color, whereas the administrative boundary with the most points will have a red color. This array should contain at least 4 elements (2 pairs).
* `legend_title`: Only for `chropleth` maps, the title of the legend.
* `popup`: Only for `choropleth` and `marker`, used to display a popup containing text describing the point that the user has clicked on the map, an array of objects having the following properties:
  * `column`: The name of the column containing the selected value
  * `type`: Optional, `string` or `url`, if `url` the url will be clickable in the popup. Default: `string`
  * `style`: Optional, a string containing css that will be used to style this value
  * `prefix`: Optional, a string that will be displayed in front of the actual value
