![Logo](/src/images/logo.png)

# DataClic

Simple web tool to export national open data for your city / local authority. Try it at [dataclic.fr](https://dataclic.fr) !

## Install / develop

This tool is open source, you are free to reuse it, and we encourage you to contribute to its development. [Install and development documentation is here](DEVELOP.md).

## Contributors

DataClic is made possible thanks to great people working on it:

- Jean-Marie Bourgogne, head of [OpenDataFrance](http://www.opendatafrance.net) association, for the idea, funding and project management
- Vincent Brunet, developer, for allowing more runner operations for themes and adding air quality and telecom infrastructure themes
- Léon Michalski, engineer student, for developing a data-visualisation module and implementing several new themes
- David Agou, Baptiste Bontoux, Clément Boudou, Hoang-Duc Duong and Jérémy Ros, [ESIEE](https://www.esiee.fr/) engineering students, for their group work on various themes
- Adrien Pavie, geomatician and developer, for initial development and maintenance of the project

## License

Copyright (c) [Adrien PAVIE](https://pavie.info) 2021-2022

Published under AGPL 3.0 license, see [LICENSE](LICENSE.txt) for complete license.
