ALTER TABLE radioelec_support ADD x_latitude VARCHAR;
ALTER TABLE radioelec_support ADD x_longitude VARCHAR;
UPDATE radioelec_support SET x_latitude = (cast("COR_NB_DG_LAT" as FLOAT) + cast("COR_NB_MN_LAT" as FLOAT) / 60.0 + cast("COR_NB_SC_LAT" as FLOAT) / 3600.0) * (CASE WHEN "COR_CD_NS_LAT" = 'N' THEN 1 ELSE -1 END);
UPDATE radioelec_support SET x_longitude = (cast("COR_NB_DG_LON" as FLOAT) + cast("COR_NB_MN_LON" as FLOAT) / 60.0 + cast("COR_NB_SC_LON" as FLOAT) / 3600.0) * (CASE WHEN "COR_CD_EW_LON" = 'E' THEN 1 ELSE -1 END);
