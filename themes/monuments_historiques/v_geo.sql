DROP MATERIALIZED VIEW IF EXISTS v_geo_monuments_historiques;
CREATE MATERIALIZED VIEW v_geo_monuments_historiques AS
	SELECT
        (CASE
            WHEN (longitude IS NOT NULL) AND (latitude IS NOT NULL) THEN ST_SetSRID(ST_MakePoint(longitude::float, latitude::float), 4326)
            WHEN coordonnees IS NOT NULL THEN ST_SetSRID(ST_MakePoint(SPLIT_PART(coordonnees, ',', 1)::float, SPLIT_PART(coordonnees, ',', 2)::float), 4326)
            WHEN x_commune_code IS NOT NULL THEN (SELECT geo_center FROM communes WHERE code = x_commune_code) 
        END)::geometry(point, 4326) as geom,
        appellation_courante as name,
        coalesce(description, historique) AS description,
        statut,
		x_commune_code,
		x_epci_code,
		x_departement_code,
		x_region_code
	FROM monuments_historiques
    WHERE lower(unaccent(date_de_protection)) like '%classe%'
	;

CREATE INDEX "v_geo_monuments_historiques.geom_idx" ON v_geo_monuments_historiques USING gist (geom);
CLUSTER v_geo_monuments_historiques USING "v_geo_monuments_historiques.geom_idx";