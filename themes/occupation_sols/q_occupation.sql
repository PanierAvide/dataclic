SELECT
	annee AS annee,
	100 * sum(clc_1) / (sum(clc_1) + sum(clc_2) + sum(clc_3) + sum(clc_4) + sum(clc_5)) AS p_clc_1,
	100 * sum(clc_2) / (sum(clc_1) + sum(clc_2) + sum(clc_3) + sum(clc_4) + sum(clc_5)) AS p_clc_2,
	100 * sum(clc_3) / (sum(clc_1) + sum(clc_2) + sum(clc_3) + sum(clc_4) + sum(clc_5)) AS p_clc_3,
	100 * sum(clc_4) / (sum(clc_1) + sum(clc_2) + sum(clc_3) + sum(clc_4) + sum(clc_5)) AS p_clc_4,
	100 * sum(clc_5) / (sum(clc_1) + sum(clc_2) + sum(clc_3) + sum(clc_4) + sum(clc_5)) AS p_clc_5
FROM occupation_sols
WHERE (<ADMTYPE> = <ADMCODE> OR <ADMCODE> IS null)
GROUP BY
	annee
ORDER BY annee ASC;