DELETE FROM occupation_sols
WHERE clc_1 !~ '^[+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)$'
	OR clc_2 !~ '^[+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)$'
	OR clc_3 !~ '^[+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)$'
	OR clc_4 !~ '^[+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)$'
	OR clc_5 !~ '^[+-]?([0-9]+([.][0-9]*)?|[.][0-9]+)$';

ALTER TABLE occupation_sols
ALTER COLUMN clc_1 TYPE float USING clc_1::float,
ALTER COLUMN clc_2 TYPE float USING clc_2::float,
ALTER COLUMN clc_3 TYPE float USING clc_3::float,
ALTER COLUMN clc_4 TYPE float USING clc_4::float,
ALTER COLUMN clc_5 TYPE float USING clc_5::float;