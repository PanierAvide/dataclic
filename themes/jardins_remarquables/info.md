Mis en place en 2004, le label « Jardin remarquable » distingue des jardins et des parcs présentant un intérêt culturel, esthétique, historique ou botanique, qu'ils soient publics ou privés.

Ce label de qualité est attribué par le [Ministère de la Culture](https://www.data.gouv.fr/fr/datasets/liste-des-jardins-remarquables/) pour une durée de 5 ans renouvelable. Il donne lieu à des avantages divers et notamment à une signalisation sur les routes et autoroutes, selon le même processus que les édifices protégés au titre des monuments historiques.
