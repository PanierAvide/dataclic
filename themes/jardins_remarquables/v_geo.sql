drop materialized view if exists v_geo_jardins;
create materialized view v_geo_jardins as
	select
		nom_du_jardin as name,
		descriptif as description,
		site_web as website,
		x_commune_code,
		x_epci_code,
		x_departement_code,
		x_region_code,
		ST_POINT(coordonnees_y::float, coordonnees_x::float)::geometry(point, 4326) as geom
	from jardins_remarquables
	;

create index "v_geo_jardins.geom_idx" on v_geo_jardins USING gist (geom);
CLUSTER v_geo_jardins USING "v_geo_jardins.geom_idx";
