#!/bin/bash

#
# Exec script for Budget theme
#

echo "PROGRESS=5"

cd `dirname $0`

inputfilename=`basename $1`
inputfilename=${inputfilename%.*}

# Clean-up on failure
cleanfail() {
	rm -rf "$DATA_FOLDER"/${inputfilename}*
	exit 1
}
trap 'cleanfail' ERR

# Run anonymizer
anoninfolder="$DATA_FOLDER/${inputfilename}_anon_in"
anonoutfolder="$DATA_FOLDER/${inputfilename}_anon_out"
mkdir -p $anoninfolder
mkdir -p $anonoutfolder
cp $1 $anoninfolder
cd ./lib_anon/
./bin/anon-doc-budg.js --in $anoninfolder --out $anonoutfolder
rm -rf "$anoninfolder"
echo "PROGRESS=50"

# Run totem2csv
cd ../lib_totem/totem2csv
totemlogfile="$DATA_FOLDER/$inputfilename.txt"
source ./env/bin/activate
./totem2csv.sh $anonoutfolder/* | tee "$totemlogfile"
deactivate
echo "PROGRESS=80"

# Create a ZIP from all exported CSV
csvfiles=`grep "Extracting" $totemlogfile | sed -r 's/Extracting sheet .+? to file //g'`
zipfile="$DATA_FOLDER/$inputfilename.zip"
mv $anonoutfolder/$inputfilename ${inputfilename}.anon.xml
zip -r $zipfile $csvfiles $inputfilename.ods ${inputfilename}.anon.xml
echo "PROGRESS=85"

# Clean-up filenames in ZIP
for f in $csvfiles; do
	printf "@ $f\n@=$(echo $f | sed -r 's/budget_[a-z0-9_]+/budget/g')\n" | zipnote -w $zipfile
done
printf "@ $inputfilename.ods\n@=budget.ods\n" | zipnote -w $zipfile
printf "@ ${inputfilename}.anon.xml\n@=budget.xml\n" | zipnote -w $zipfile

# Cleanup totem2csv folder
rm -rf "$anonoutfolder"
rm "$totemlogfile" "$inputfilename.ods" $csvfiles ${inputfilename}.anon.xml
echo "PROGRESS=99"

# Let NodeJS runner know where output file is located
echo "OUTPUT_FILE=$zipfile"
