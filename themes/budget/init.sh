#!/bin/bash

#
# Init script for Budget theme
# Dependencies : Python (3), pip, xmlstarlet, zip, zipnote
#

set -e
cd `dirname $0`

echo ""
echo "#######################################################"
echo "# Retrieve latest budgetary norms"
echo ""

cd lib_totem/norme-budgetaire-downloader/
npm install
sed -i 's#convertXmlToUtf8(`#//convertXmlToUtf8(`#g' src/index.ts
rm -rf ./output/*
npm run build
npm run run

echo ""
echo "#######################################################"
echo "# Install converter dependencies"
echo ""

cd ../totem2csv/
python3 -m venv ./env
source ./env/bin/activate
pip install -r requirements.txt
deactivate

echo ""
echo "#######################################################"
echo "# Install anonymizer dependencies"
echo ""

cd ../../lib_anon/
npm install
