select
	secteur as label,
	count(*) as value
from
	effectif_scolaire
where
    (<ADMTYPE> = <ADMCODE> or <ADMCODE> is null)
group by secteur
order by secteur desc;