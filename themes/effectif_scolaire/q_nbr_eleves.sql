select
	"rentrée scolaire" as year,
	sum("nombre d'élèves en cp hors ulis"::INT) as cp,
	sum("nombre d'élèves en ce1 hors ulis"::INT) as ce1,
	sum("nombre d'élèves en ce2 hors ulis"::INT) as ce2,
	sum("nombre d'élèves en cm1 hors ulis"::INT) as cm1,
	sum("nombre d'élèves en cm2 hors ulis"::INT) as cm2
from
	effectif_scolaire
where
	(<ADMTYPE> = <ADMCODE> or <ADMCODE> is null)
group by "rentrée scolaire"
order by "rentrée scolaire" asc;