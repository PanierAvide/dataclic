create materialized view v_elections_candidats as
with js as (
	select
		type_election,
		tour,
		year,
		exprimes::int,
		x_commune_code,
		x_epci_code,
		x_arrondissement_code,
		x_departement_code,
		x_region_code,
		json_array_elements(cast(candidats as json))->>'prenom' as first_name,
		json_array_elements(cast(candidats as json))->>'nom' as name,
		json_array_elements(cast(candidats as json))->>'nuance' as nuance,
		(json_array_elements(cast(candidats as json))->>'voix')::int as voix
	from elections
	where x_commune_code is not null
		AND exprimes::int > 0
)
select
	CONCAT_WS(' ', first_name, name) as name,
	nuance,
	sum(exprimes) as exprimes,
	sum(voix) as voix,
	type_election,
	tour,
	year,
	x_commune_code,
	x_epci_code,
	x_arrondissement_code,
	x_departement_code,
	x_region_code
from js
group by
	type_election,
	year,
	tour,
	nuance,
	name,
	first_name,
	x_commune_code, 	
	x_epci_code,
	x_arrondissement_code,
	x_departement_code,
	x_region_code
;

create index "v_elections_candidats.type_election_year_tour_idx" on v_elections_candidats (type_election, year, tour);
create index "v_elections_candidats.v_elections_candidats_x_commune_code_idx" on v_elections_candidats (x_commune_code);
create index "v_elections_candidats.v_elections_candidats_x_epci_code_idx" on v_elections_candidats (x_epci_code);
create index "v_elections_candidats.v_elections_candidats_x_arrondissement_code_idx" on v_elections_candidats (x_arrondissement_code);
create index "v_elections_candidats.v_elections_candidats_x_departement_code_idx" on v_elections_candidats (x_departement_code);
create index "v_elections_candidats.v_elections_candidats_x_region_code_idx" on v_elections_candidats (x_region_code);
