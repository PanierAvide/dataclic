SELECT
    concat(year, ' Tour ', tour) as x,
    SUM(exprimes::INT) as exprimes,
    SUM(abstentions::INT) as abstentions,
    SUM(blancs::INT) as blancs,
    SUM(nuls::INT) as nuls
FROM v_elections_stats
WHERE (<ADMTYPE> = <ADMCODE> or <ADMCODE> is null)
        AND type_election = '<TYPE_ELECTION>'
GROUP BY year, tour
ORDER BY year::INT, tour::INT;
