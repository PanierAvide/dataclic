"""
This script is made to extract informations about french elections and create a csv ready to import in a database.
To achieve this we first parse the header (included in the input csv or provided as an argument) and we identify
the fields should be used to build a "bureau de vote" (the BVote class) by using the BVoteParser class.
Once this is done, we use the CandidatParser to identify the indexes of the columns that are related to a "candidat".
Finally, we parse the input csv and create an output csv in utf8, which contains one "bureau de vote" per line.

To try the script on your csv, you can use the "--dry_run" option, which will run the script on the first 5 lines.

If a field is not identified for your csv, you can modify the list of strings used in the BVoteParser and CandidatParser,
but you should make sure that the string you add is not already used for another field
(there is a check to prevent the script from running if two fields have the same possible_names).

If this is not possible, you can always pass a custom header as argument and it will overwrite the header of the csv.

To get a of all possible options, run this script with parameter "-h" or "--help".
This script has been tested with python 3.8 and 3.10
"""

import sys
import argparse
from typing import Union, List, Dict, Set
from pathlib import Path

def parse_code_dep(code_str: str) -> str:
    """
    A function to convert the dep codes from the format used in the election csv's to INSEE codes
    """
    try:
        code_int = int(code_str)
        if code_int <= 0 | code_int >= 1000:
            raise Exception(f"Code departement is wrong: {code_str}")
        elif code_int >= 100:
            # Handling outre mer
            return f"{code_int:03d}"
        else:
            # Handling metropolitan France
            return f"{code_int:02d}"
    except ValueError:
        if code_str=="2A" or code_str=="2B": # Corsica
            return code_str
        if code_str == "ZA": # Guadeloupe
            return "971"
        if code_str == "ZB": # Martinique
            return "972"
        if code_str == "ZC": # Guyane
            return "973"
        if code_str == "ZD": # La Reunion
            return "974"
        if code_str == "ZM": # Mayotte
            return "976"
        if code_str == "ZN": # Nouvelle-Calédonie
            return "988"
        if code_str == "ZS": # Saint-Pierre-et-Miquelon
            return "975"
        if code_str == "ZP": # Polynesie Francaise
            return "987"
        if code_str == "ZX": # Saint-Martin / Saint-Barthélemy
            return "97"
        if code_str == "ZW": # Wallis et Futuna
            return "986"
        if code_str == "ZZ":
            return "ZZ"

        raise Exception(f"Unknown dep code: {code_str}")


def parse_code_coc(code_dep: str, code_coc_str: str) -> str:
    """
    A function to parse the correct code_coc.
    """
    try:
        code_coc_int = int(code_coc_str)
        if code_coc_int >= 1000 or code_coc_int < 0:
            raise Exception(f"Unable to parse code commune: {code_coc_str}")
        if len(code_dep) == 2:
            # Handling metropolitan France or corsica // Somehow works for Saint-Martin / Saint-Barthélemy
            return f"{code_dep}{int(code_coc_str):03d}"
        if len(code_dep) == 3 and len(code_coc_str) == 3 :
            # Handling outre mer
            # It can happen that the commune code is 3 digits long when it should be two,
            # in this case the first digit of code_coc should be the same as the last digit of code_dep
            if code_coc_str[0] == code_dep[-1]:
                return f"{code_dep}{int(code_coc_str[1:]):02d}"
            raise Exception(f"Could not parse code commune code_coc: {code_coc_str}, dep.code: {code_dep}")
        raise Exception(f"Wrong dep code: {code_dep}")
    except ValueError:
        raise ValueError(f"Unknown code commune: {code_coc_str}")


def unaccent(s: str) -> str:
    """
    Removes the french accents from a string (and double quotes)
    """
    return s.replace('é', 'e').replace('è', 'e').replace('ê', 'e').replace('ô', 'o').replace('"', '')


def cleanString(s: str) -> str:
    """
    Removes one comma and one % from a string, useful for parsing french floats
    """
    return s.replace(",", ".", 1).replace("%", "", 1)


class Field():
    """
    A Field is used to parse a column of a csv.
    It is initialized with a name that will be used in the header of the output csv,
    a list of the possible names it could have in the input csv, or a forced value.
    Once the index of the field in the header of the input csv, we can use the instance
    to get the value in a given line (or the forced value) in the type that was set at init.
    """
    name: str
    name_in_header: str
    possible_names: List[str]
    index: int = -1
    field_type: str
    forced_value = None

    def __init__(self, name, possible_names: List[str], field_type=str):
        self.name = name
        self.possible_names = possible_names
        self.field_type = field_type
        self.forced_value = None


    def parseIndex(self, v: str, index: int) -> None:
        """
        A method to try to identify this field in the header.
        """
        # Warning: calling isParsed here will not work for ComputableFields
        if (self.index == -1) and unaccent(v.lower()) in self.possible_names:
            self.index = index
            self.name_in_header = v

    def isParsed(self) -> bool:
        return (self.index != -1) or (self.forced_value is not None)

    def getValue(self, line: List[str], i: int = 0) -> str:
        """
        getValue returns the value this field has in a given line
        with a correct type or an empty string. i can be used as an offset (useful for the candidates)
        """
        if self.forced_value is not None:
            return self.forced_value

        if self.isParsed():
            if (self.field_type == float) or (self.field_type == int):
                return self.field_type(cleanString(line[self.index + i]))
            return self.field_type(line[self.index + i]).replace('"', '')
        else:
            return ''

    def __str__(self) -> str:
        if self.forced_value is not None:
            return f"{self.name} => forced to {self.forced_value}"
        if self.isParsed():
            return f"{self.name} => {self.name_in_header} at index {self.index}"
        else:
            return f"{self.name} could NOT be identified in the header"

    def __hash__(self):
        """
        Return a hash of this field, needed to use fields as dict keys.
        """
        return hash(self.name)

    def __repr__(self) -> str:
        return self.name


class ComputableField(Field):
    """
    A computable field works like a "normal" field,
    except that if it can't be identified in the header, the value will be equal to 100 * field1 / field2
    Warning:    the field f2 should not be in candidat parser, it HAS TO BE in the bVote parser.
                this is because the offset will be wrong, because candidats parser does not use fixed offsets.
    """
    f1: Field
    f2: Field

    def __init__(self, name, possible_names: List[str], f1: Field, f2: Field):
        Field.__init__(self, name, possible_names, field_type=float)
        self.f1 = f1
        self.f2 = f2

    def getValue(self, line: List[str], i=0) -> Union[str, float]:
        if self.index != -1:
            # The value is in the file, return it
            return float(cleanString(line[self.index + i]))
        elif self.isComputable():
            # Note: f2 can NOT be in candidats header
            f2_value = self.f2.getValue(line, 0)
            if f2_value == 0:
                # Avoid division by 0
                return 0.0
            return round(100 * self.f1.getValue(line, i) / self.f2.getValue(line, 0), 2)
        else:
            return ''

    def parseIndex(self, v: str, index: int) -> None:
        super().parseIndex(v, index)

    def isComputable(self) -> bool:
        return (self.f1.isParsed() and self.f2.isParsed())

    def isParsed(self) -> bool:
        return (super().isParsed()) or self.isComputable()

    def __str__(self) -> str:
        if (self.index == -1) and self.isComputable():
            return f"{self.name} was not found in the header but is computable"
        elif not self.isParsed():
            return f"{self.name} was not found in the header and IS NOT computable"
        else:
            return super().__str__()

    def __hash__(self) -> int:
        """
        Return a hash of this field, needed to use fields as dict keys.
        """
        return hash((self.name, self.f1.__hash__(), self.f2.__hash__()))

    def __repr__(self) -> str:
        return super().__repr__()



class BVote():
    """
    An instance of this class will hold the information parsed for one "bureau de vote".
    """
    tour: str
    commune_code: str
    bvote_code: str
    fields_values: List

    candidats: List[str]

    def __init__(self, tour: str, commune_code: str, bvote_code: str, fields: List):
        self.tour = tour
        self.commune_code = commune_code
        self.bvote_code = bvote_code
        self.fields_values = fields

        self.candidats = []

    def get_key(self) -> tuple:
        """
        Returns a tuple to use as a key in a dict.
        """
        return (self.tour, self.commune_code, self.bvote_code)

    def appendCandidats(self, candidats: List[str]) -> None:
        for c in candidats:
            self.candidats.append(c)

    def format(self, delimiter: str) -> str:
        """
        Return the string resulting of the concatenation of all fields
        """
        candidats_str = f'"[{",".join(self.candidats)}]"'
        r = []
        for f in self.fields_values:
            if f is None:
                r.append('')
            else:
                r.append(str(f))
        r.append(candidats_str)
        return delimiter.join(r)


class BVoteParser():
    """
    The parser for "bureau de vote".
    """
    type_election = Field("type_election", ["type_election"])
    tour = Field("tour", ["tour", "n° tour"])
    year = Field("year", ["annee"])
    dep_code = Field("code_departement", ["code du departement", "code departement"])
    dep_name = Field("departement", ["libelle du departement", "departement"])

    commune_code = Field("code_commune", ["code de la commune", "code commune"])
    commune_name = Field("commune", ["libelle de la commune", "commune", "nom de la commune"])

    bvote_code = Field("n_bureau", ["code du b.vote", "code b.vote", "n° de bureau de vote"])
    inscrits = Field("inscrits", ["inscrits"], int)
    abstentions = Field("abstentions", ["abstentions"], int)
    votants = Field("votants", ["votants"], int)
    nuls = Field("nuls", ["nuls"], int)
    blancs = Field("blancs", ["blancs"], int)
    exprimes = Field("exprimes", ["exprimes"], int)

    normal_fields = [type_election, tour, year, dep_code, dep_name, commune_code, commune_name, bvote_code, inscrits, abstentions, votants, nuls, blancs, exprimes]

    # These fields will not be written to the output csv, the admin mapping is responsible for this,
    # this might change as they might be needed to correct some of the errors of code_commune
    ignored_fields = [dep_name, commune_name]

    p_abs_insc = ComputableField("% Abs/Ins", ["% abs/ins"], abstentions, inscrits)
    p_vot_insc = ComputableField("% Vot/Ins", ["% vot/ins"], votants, inscrits)
    p_bl_insc = ComputableField("% Blancs/Ins", ["% blancs/ins"], blancs, inscrits)
    p_bl_vot = ComputableField("% Blancs/Vot", ["% blancs/vot"], blancs, votants)
    p_nul_insc = ComputableField("% Nuls/Ins", ["% nuls/ins"], nuls, inscrits)
    p_nul_vot = ComputableField("% Nuls/Vot", ["% nuls/vot"], nuls, inscrits)
    p_exp_insc = ComputableField("% Exp/Ins", ["% exp/ins"], exprimes, inscrits)
    p_exp_vot = ComputableField("% Exp/Vot", ["% exp/vot"], exprimes, votants)

    computable_fields: List[ComputableField] = [p_abs_insc, p_vot_insc, p_bl_insc, p_bl_vot, p_nul_insc, p_nul_vot, p_exp_insc, p_exp_vot]

    all_fields: List[Field] = normal_fields + computable_fields

    parsed_fields: List[Field] = []     # All fields that can be parsed in the csv, sorted by the index
    unparsed_fields: List[Field] = []   # All fields that cannot be parsed in the csv

    def __init__(self, header: List[str], type_election, tour, year):
        # The following fields can be forced to a value, as they are rarely present in the input csv.
        if type_election is not None:
            self.type_election.forced_value = type_election
        if tour is not None:
            if tour == "_":
                self.tour.forced_value = ""
            else:
                self.tour.forced_value = tour
        if year is not None:
            self.year.forced_value = year

        # Try to parse all fields
        for i in range(0, len(header)):
            for f in self.all_fields:
                f.parseIndex(header[i], i)

        # Build the list of all parsed fields (and the list of all the unparsed ones)
        for f in self.all_fields:
            if f.isParsed():
                self.parsed_fields.append(f)
            else:
                self.unparsed_fields.append(f)

        # Sort parsed fields
        self.parsed_fields = sorted(self.parsed_fields, key=lambda f: f.index)

        print("\nIgnored Fields in bVote, this might be normal if the fields should be in the candidats parser:")
        for i in range(0, len(header)):
            was_found = False
            for f in self.parsed_fields:
                if f.index == i:
                    was_found = True
            if not was_found:
                print(header[i])

    def parse(self, line: List[str]) -> BVote:
        """
        Given a line of the csv as a list of strings, extract the value of each field and return a BVote instance.
        """
        parsed_line: Dict[Field, str] = {}
        for f in self.parsed_fields:
            parsed_line[f] = f.getValue(line)

        # Fix the dep_code and the commune code
        parsed_line[self.dep_code] = parse_code_dep(parsed_line[self.dep_code])
        parsed_line[self.commune_code] = parse_code_coc(parsed_line[self.dep_code], parsed_line[self.commune_code])

        r = []
        for f in self.all_fields:
            if f not in self.ignored_fields:
                r.append(parsed_line.get(f, None))
        return BVote(parsed_line.get(self.tour, None), parsed_line[self.commune_code], parsed_line[self.bvote_code], r)

    def formatHeader(self) -> List[str]:
        """
        Returns a list of all the field names that should be included in the header.
        """
        r = []
        for f in self.all_fields:
            if f not in self.ignored_fields:
                r.append(f.name)
        return r

    def getHighestIndex(self) -> int:
        """
        Returns the highest index of all parsed fields. (Used for the parsing of the candidats)
        """
        return self.parsed_fields[-1].index



class CandidatParser():
    """
    The parser for the candidats.
    Unlike the BVote parser, it VERY important to parse as much fields as possible, even if they will not be included in the output csv.
    """
    # Normal Fields
    nom = Field("nom", ["nom", "nom tete de liste", "binome", "nom du candidat", "nom du candidat tete de liste"])
    prenom = Field("prenom", ["prenom", "prenom du candidat", "prenom du candidat tete de liste"])
    sexe = Field("sexe", ["sexe"])
    n_panneau = Field("n°panneau", ["n.pan", "n°panneau", "n.pan.", "n° de depot de la liste", "n° de depot du candidat"])
    voix = Field("voix", ["voix", "nombre de voix", "nombre de voix du candidat"], int)
    nuance = Field("nuance", ["nuance", "nuance liste", "code nuance", "code nuance du candidat", "code nuance de la liste"])
    normal_fields = [nom, prenom, sexe, n_panneau, voix, nuance]

    # Ignored Fields (they will not be included in the output csv)
    ignored_fields: List[Field] = [
        Field("n°liste", ["n°liste"]),
        Field("libelle_abrege_liste", ["libelle abrege liste"]),
        Field("libelle_etendu_liste", ["libelle etendu liste"]),
    ]

    bVote: BVoteParser
    p_voix_insc: ComputableField
    p_voix_exp: ComputableField

    computable_fields: List[ComputableField]
    all_fields: List[Field]

    offset_in_header: int # The start of candidats in the header
    parsed_fields: List[Field] = []     # All fields that can be parsed in the csv, sorted by the index
    unparsed_fields: List[Field] = []   # All fields that cannot be parsed in the csv

    def __init__(self, header: List[str], bVote: BVoteParser) -> None:
        self.bVote = bVote
        self.offset_in_header = bVote.getHighestIndex() + 1
        # Add computable fields
        self.p_voix_insc = ComputableField("% Voix/Ins", ["% voix/ins"], self.voix, bVote.inscrits)
        self.p_voix_exp = ComputableField("% Voix/Exp", ["% voix/exp"], self.voix, bVote.exprimes)
        self.computable_fields: List[ComputableField] = [self.p_voix_insc, self.p_voix_exp]

        self.all_fields: List[Field] = self.normal_fields + self.computable_fields + self.ignored_fields

        # Parse header
        header_candidat = header[self.offset_in_header:]
        for i in range(0, len(header_candidat)):
            for f in self.all_fields:
                f.parseIndex(header_candidat[i], i + self.offset_in_header)

        for f in self.all_fields:
            if f.isParsed():
                self.parsed_fields.append(f)
            else:
                self.unparsed_fields.append(f)

        # Sort parsed_fields
        self.parsed_fields = sorted(self.parsed_fields, key=lambda f: f.index)

    def parse(self, line: List[str]) -> List[str]:
        """
        Parse a line that can contain multiple candidats.
        We compute the number of fields each candidats should have based on the smallest parsed index and the biggest.
        """
        line_candidat = line[self.offset_in_header:]
        step = (self.parsed_fields[-1].index - self.parsed_fields[0].index) + 1 # +1 because we start at 0

        parsed_line: List[str] = []
        for i in range(0, len(line_candidat), step):
            c = []
            for f in self.all_fields:
                if f not in self.ignored_fields:
                    c.append(f'""{f.name}"":""{f.getValue(line, i=i)}""')
            parsed_line.append('{' + ','.join(c) + '}')

        return parsed_line

def checkParsers(bVoteParser: BVoteParser, candidatParser: CandidatParser) -> None:
    """
    This function makes sure that each field of the parser has no duplicate possible names,
    prints a summary of all the field that were parsed with their index and the fields that
    could not be identified in the header, finally it checks if each identified has a unique index.
    """
    # Check that each field has unique possible_names
    for parser in (bVoteParser, candidatParser):
        names: Set[str] = set()
        for f in parser.all_fields:
            for n in f.possible_names:
                if n not in names:
                    names.add(n)
                else:
                    sys.exit(f"ERROR Duplicate name in possible_names: {n} of field {f}")

    # Check that each field has a unique index
    indexes: Set[int] = set()
    duplicate_indexes: List[int] = []
    for parser in (bVoteParser, candidatParser):
        for f in parser.parsed_fields:
            if f.index == -1:
                continue
            if f.index in indexes:
                duplicate_indexes.append(f.index)
            else:
                indexes.add(f.index)

    print("\nParsed fields:")
    for parser in (bVoteParser, candidatParser):
        print(f"\n{type(parser).__name__}")
        for f in parser.parsed_fields:
            print(f)

    for parser in (bVoteParser, candidatParser):
        if len(parser.unparsed_fields) == 0:
            print(f"\nNo unparsed field in {type(parser).__name__}")
        else:
            print(f"\nUnparsed fields in {type(parser).__name__}:")
            for f in parser.unparsed_fields:
                print(f)

    if len(duplicate_indexes) == 0:
        print("\nNo duplicate fields were identified.")
    else:
        print("\nWarning duplicated fields were identified:")
        for i in duplicate_indexes:
            for parser in (bVoteParser, candidatParser):
                for f in parser.parsed_fields:
                    if i == f.index:
                        print("DUPLICATE:", f)
        sys.exit("Duplicate fields were identified, aborting.")

def main():
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument("--dry_run", help="Run the script over 5 lines", action="store_true")
    arg_parser.add_argument("--input", help="The path to the input file", type=str)
    arg_parser.add_argument("--output", help="The path to the output file", type=str)
    arg_parser.add_argument("--output_mode", help="Output mode (overwrite or append in existing output file)", type=str, default="append")
    arg_parser.add_argument("--nlines", help="The number of lines to delete at the beginning of the input file", type=int, default=0)
    arg_parser.add_argument("--delimiter", help="The delimiter used in the csv given as input, for a tab use string tab", type=str, default=";")
    arg_parser.add_argument("--encoding", help="The encoding of the input_file", type=str, default="utf-8")
    arg_parser.add_argument("--type_election", help="The type of the election: presidentielle, regionale, etc..", type=str, default=None)
    arg_parser.add_argument("--year", help="The year of the election", type=str, default=None)
    arg_parser.add_argument("--tour", help="The round number, if the election does not have multiple rounds, use an underscore ", type=str, default=None)
    arg_parser.add_argument("--header", help="A string containing the header that will be used for the parsing", type=str, default=None)
    arg_parser.add_argument("--no_log_combine", help="Do not log when combining candidates", type=str, default="")
    args = arg_parser.parse_args()

    # Use "official name" of encoding
    if args.encoding == "ANSI":
        args.encoding = "ISO-8859-1"

    # Passing directly "\t" as an argument is not always possible,
    # because some shells will automaticaly duplicate the back-slash
    if args.delimiter == "tab":
        args.delimiter = "\t"

    # Fix the no_log_combine arg
    if args.no_log_combine in ("", "True", "1"):
        args.no_log_combine = True
    else:
        args.no_log_combine = False

    bVoteParser: BVoteParser
    candidatParser: CandidatParser

    parsed_bvotes: Dict[tuple, BVote] = {} # A dictionary of all the bVote that have been parsed
    parsing_errors: Dict[int, Exception] = {} # A dictionary of all the errors that happened during the parsing, with their line number as key

    print("Handling:", args.input)
    with open(args.input, encoding=args.encoding) as inputf:
        headerIsParsed = False
        line_count = 0
        for line in inputf:
            line_count += 1
            if line_count <= args.nlines:
                continue

            line = line.rstrip("\t+\r\n") # Remove eol and all tabs before it
            line = line.split(args.delimiter)
            if not headerIsParsed:
                if args.header is not None:
                    # A custom header was passed as argument, use it
                    header = args.header.split(args.delimiter)
                else:
                    header = line

                bVoteParser = BVoteParser(header, args.type_election, args.tour, args.year)
                candidatParser = CandidatParser(header, bVoteParser)

                # Check if the parsers can be used
                checkParsers(bVoteParser, candidatParser)

                headerIsParsed = True
                if header == line:
                    # This line was the header, don't parse it
                    continue

            # Parse the line
            try:
                bVote = bVoteParser.parse(line)
                candidats = candidatParser.parse(line)
                key = bVote.get_key()
                # Some csv use one line per candidat, which means that we have to combine
                # the candidats to have a single bVote instance per actual "bureau de vote"
                if parsed_bvotes.get(key, None) is None:
                    parsed_bvotes[key] = bVote
                elif not args.no_log_combine:
                    print(f"Line {line_count}: BVote already existed, combine them")

                parsed_bvotes[key].appendCandidats(candidats)
            except Exception as e:
                parsing_errors[line_count] = e

            if args.dry_run:
                if line_count > (args.nlines + 5):
                    break

    for k in parsing_errors.keys():
        print(f"Line {k} was ignored: {parsing_errors[k]}")

    # Create a list of all lines that should be written to the output csv.
    outputLines: List[str] = []
    # The mode that will be used to write to the output file, by default append
    write_mode = "a"
    if (not Path(args.output).is_file()) or args.dry_run or args.output_mode == "overwrite":
        # If the file does not exists, we add the header and set the write_mode
        formated_header = bVoteParser.formatHeader()
        formated_header.append("candidats")
        outputLines.append(";".join(formated_header) + "\n")
        write_mode = "w"

    for key in parsed_bvotes:
        outputLines.append(parsed_bvotes[key].format(";") + "\n")


    with open(args.output, mode=write_mode, encoding="utf-8") as outputf:
        outputf.writelines(outputLines)


if __name__ == "__main__":
    main()
