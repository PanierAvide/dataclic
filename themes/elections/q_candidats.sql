with q as (
	select
		CASE
			WHEN '<TYPE_ELECTION>' = 'presidentielles' OR ('<ADMTYPE>' = 'x_commune_code' OR '<ADMTYPE>' = 'x_epci_code')
				THEN "name"
				ELSE "nuance"
		end as "name",
		voix,
		exprimes::numeric
	from v_elections_candidats
	where (<ADMTYPE> = <ADMCODE> or <ADMCODE> is null)
		and type_election = '<TYPE_ELECTION>'
		and year = '<YEAR>'
		and tour = '<TOUR>'
) select 
	name,
	100 * sum(voix) / sum(exprimes) as percentage
from q
group by name
order by percentage DESC;