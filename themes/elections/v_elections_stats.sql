create materialized view v_elections_stats as
	select
		type_election,
		year,
		tour,
		SUM(exprimes::INT) as exprimes,
		SUM(abstentions::INT) as abstentions,
		SUM(blancs::INT) as blancs,
		SUM(nuls::INT) as nuls,
		x_commune_code,
		x_epci_code,
		x_arrondissement_code,
		x_departement_code,
		x_region_code 
	from elections
	where
		abstentions is not null
		AND blancs is not null
		AND nuls is not null
	group by
		x_commune_code,
		year,
		tour,
		type_election,
		x_epci_code,
		x_arrondissement_code,
		x_departement_code,
		x_region_code
	;
	
create index "v_elections_stats.type_election" on v_elections_stats (type_election);
create index "v_elections_stats.v_elections_stats_x_commune_code_idx" on v_elections_stats (x_commune_code);
create index "v_elections_stats.v_elections_stats_x_epci_code_idx" on v_elections_stats (x_epci_code);
create index "v_elections_stats.v_elections_stats_x_arrondissement_code_idx" on v_elections_stats (x_arrondissement_code);
create index "v_elections_stats.v_elections_stats_x_departement_code_idx" on v_elections_stats (x_departement_code);
create index "v_elections_stats.v_elections_stats_x_region_code_idx" on v_elections_stats (x_region_code);