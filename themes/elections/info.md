Le [Ministère de l’Intérieur](https://www.data.gouv.fr/fr/organizations/ministere-de-l-interieur/) propose de nombreux jeux de données recensant les résultats des différentes élections (présidentielles, législatives, municipales etc.) pour chaque commune de France. Ces données sont consultables [sur data.gouv.fr](https://www.data.gouv.fr/fr/pages/donnees-des-elections/).

Le formatage des données officielles n'est pas toujours identique en fonction du type d'élections ou d'une année à l'autre. DataClic récupère les données officielles, identifie les colonnes intéressantes et reformatte le document pour en proposer une version consolidée et utilisable via la barre de recherche.

Les données exportent comportent les informations suivantes pour chaque bureau de vote :
- Code INSEE de la commune
- Numéro du bureau de vote
- Nombre d'inscrits / votants / exprimés, et dans certains cas abstentions / nuls / blancs
- Taux (%) calculés entre les champs :
  - Abstentions / Inscrits (taux d'abstention)
  - Votants / Inscrits (taux de participation)
  - Blancs / Inscrits
  - Blancs / Votants
  - Nuls / Inscrits
  - Nuls / Votants
  - Exprimés / Inscrits
  - Exprimés / Votants
- Liste des candidats (nom, prénom, sexe) et le nombre de voix obtenus
- Type d'élection, année et numéro du tour

Le module comporte les résultats des élections suivantes :

- __Présidentielles__ (2 tours) : 2002, 2007, 2012, 2017 et 2022
- __Régionales__ (2 tours) : 2010 et 2021
- __Départementales__ (2 tours) : 2021
- __Municipales__ (2 tours) : 2014 et 2020
- __Européennes__ : 2009, 2014 et 2019
- __Législatives__ (2 tours) : 2012, 2017 et 2022
