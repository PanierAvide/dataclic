{
	"type": "exposer",
	"name": "Élections",
	"description": "Téléchargez les résultats des élections de votre territoire.",
	"style": {
		"icon": "fas fa-vote-yea",
		"color": "#0F3057"
	},
	"metadata": {
		"dataset": "Données des élections",
		"producer": "Ministère de l’Intérieur",
		"doc": "https://www.data.gouv.fr/fr/pages/donnees-des-elections/",
		"updates": "Ponctuelle",
		"licence": "Licence Ouverte / Open Licence version 2.0",
		"category": "Élections",
		"precision": "Bureaux de votes"
	},
	"dataviz": {
		"g1": {
			"type": "graph",
			"subtype": "bar",
			"title": "Evolution de la participation aux présidentielles 2022",
			"description": "Evolution de la participation aux deux dernières élections présidentielles (2017 et 2022).",
			"query_file": "q_evolution.sql",
			"query_constants": {
				"<TYPE_ELECTION>": "presidentielles"
			},
			"datasets": [
				{
					"label": "Nuls",
					"valueKey": "nuls",
					"color": ["rgb(255,181,90)"],
					"stack": "votes"
				}, {
					"label": "Abstentions",
					"valueKey": "abstentions",
					"color": ["rgb(253,127,111)"],
					"stack": "votes"
				}, {
					"label": "Blancs",
					"valueKey": "blancs",
					"color": ["rgb(126,176,213)"],
					"stack": "votes"
				}, {
					"label": "Exprimés",
					"valueKey": "exprimes",
					"color": ["#264653"],
					"stack": "votes"
				}
			]
		},
		"legislatives_t2": {
			"type": "graph",
			"subtype": "bar",
			"query_file": "q_candidats.sql",
			"query_constants": {
				"<TYPE_ELECTION>": "presidentielles",
				"<YEAR>": "2022",
				"<TOUR>": "2"
			},
			"title": "Voix / Exprimés % - Présidentielles 2022 (2nd tour)",
			"description": "Résultat du second tour de l’élection présidentielle 2022, en nombre de voix par rapport au nombre de voix exprimées (et valides).",
			"datasets": [{
				"labelKey": "name",
				"valueKey": "percentage"
			}]
		},
		"legislatives_t1": {
			"type": "graph",
			"subtype": "bar",
			"query_file": "q_candidats.sql",
			"query_constants": {
				"<TYPE_ELECTION>": "presidentielles",
				"<YEAR>": "2022",
				"<TOUR>": "1"
			},
			"title": "Voix / Exprimés % - Présidentielles 2022 (1er tour)",
			"description": "Résultat du premier tour de l’élection présidentielle 2022, en nombre de voix par rapport au nombre de voix exprimées (et valides).",
			"datasets": [{
				"labelKey": "name",
				"valueKey": "percentage"
			}]
		}
	},
	"runner": [
		{ "task": "download", "url": "https://www.data.gouv.fr/fr/datasets/r/52c3e6f1-2faa-4295-9f76-5b0e9cb9f7b0", "file": "2021_REG_T1.txt"},
		{ "task": "python", "script": "process_csv.py", "input": "2021_REG_T1.txt", "output": "out.csv", "output_mode": "overwrite", "delimiter":";", "encoding":"ANSI", "year": "2021", "type_election": "regionales", "tour": "1"},
		{ "task": "download", "url": "https://www.data.gouv.fr/fr/datasets/r/75221ab0-2066-4a11-bc42-b0f76d201dfe", "file": "2021_REG_T2.txt"},
		{ "task": "python", "script": "process_csv.py", "input": "2021_REG_T2.txt", "output": "out.csv", "delimiter":";", "encoding":"ANSI", "year": "2021", "type_election": "regionales", "tour": "2"},
		{ "task": "download", "url": "https://www.data.gouv.fr/fr/datasets/r/57370a9b-7fa1-465c-a051-c984fc21321f", "file": "2021_DEP_T1.txt"},
		{ "task": "python", "script": "process_csv.py", "input": "2021_DEP_T1.txt", "output": "out.csv", "delimiter":";", "encoding":"ANSI", "year": "2021", "type_election": "departementales", "tour": "1"},
		{ "task": "download", "url": "https://www.data.gouv.fr/fr/datasets/r/b34b36d9-e416-4144-8384-d101b140afaf", "file": "2021_DEP_T2.txt"},
		{ "task": "python", "script": "process_csv.py", "input": "2021_DEP_T2.txt", "output": "out.csv", "delimiter":";", "encoding":"ANSI", "year": "2021", "type_election": "departementales", "tour": "2"},
		{ "task": "download", "url": "https://www.data.gouv.fr/fr/datasets/r/248f6f21-68ad-45f3-82f5-53fffabce5f3", "file": "2020_MUN_T1.txt"},
		{ "task": "python", "script": "process_csv.py", "input": "2020_MUN_T1.txt", "output": "out.csv", "delimiter":"tab", "encoding":"ANSI", "year": "2020", "type_election": "municipales", "tour": "1"},
		{ "task": "download", "url": "https://www.data.gouv.fr/fr/datasets/r/7e641d2e-e017-43d4-9434-49d5acd44b4b", "file": "2020_MUN_T2.txt"},
		{ "task": "python", "script": "process_csv.py", "input": "2020_MUN_T2.txt", "output": "out.csv", "delimiter":";", "encoding":"ANSI", "year": "2020", "type_election": "municipales", "tour": "2"},
		{ "task": "download", "url": "https://www.data.gouv.fr/fr/datasets/r/77c4450b-7fa7-425c-84da-4f7bf4b97820", "file": "2019_EUR.txt"},
		{ "task": "python", "script": "process_csv.py", "input": "2019_EUR.txt", "output": "out.csv", "delimiter":";", "encoding":"ANSI", "year": "2019", "type_election": "europeennes", "nlines": "1", "header": "Code du département;Libellé du département;Code de la commune;Libellé de la commune;Code du b.vote;Inscrits;Abstentions;% Abs/Ins;Votants;% Vot/Ins;Blancs;% Blancs/Ins;% Blancs/Vot;Nuls;% Nuls/Ins;% Nuls/Vot;Exprimés;% Exp/Ins;% Exp/Vot;N°Liste;nuance;Libellé Etendu Liste;Nom Tête de Liste;Voix;% Voix/Ins;% Voix/Exp"},
		{ "task": "download", "url": "https://www.data.gouv.fr/fr/datasets/r/80cb1309-9147-4bae-b6e2-79877d549b50", "file": "2017_LEG_T1.txt"},
		{ "task": "python", "script": "process_csv.py", "input": "2017_LEG_T1.txt", "output": "out.csv", "delimiter": ";", "encoding":"ANSI", "year": "2017", "type_election": "legislatives", "tour": "1"},
		{ "task": "download", "url": "https://www.data.gouv.fr/fr/datasets/r/8eb61f3e-dfdf-496e-85af-2859cd7383c3", "file": "2017_LEG_T2.txt"},
		{ "task": "python", "script": "process_csv.py", "input": "2017_LEG_T2.txt", "output": "out.csv", "delimiter": ";", "encoding":"ANSI", "year": "2017", "type_election": "legislatives", "tour": "2"},
		{ "task": "download", "url": "https://www.data.gouv.fr/fr/datasets/r/c5af18ce-b673-4019-a9c3-07ee6209668c", "file": "2014_EUR.txt"},
		{ "task": "python", "script": "process_csv.py", "input": "2014_EUR.txt", "output": "out.csv", "delimiter": ";", "encoding":"ANSI", "year": "2014", "type_election": "europeennes", "nlines": "16", "no_log_combine":"True", "header": ";Code département;Code de la commune;Nom de la commune;N° de bureau de vote;Inscrits;Votants;Exprimés;N° de dépôt du candidat;Nom du candidat;Prénom du candidat;Code nuance du candidat;Nombre de voix du candidat" },
		{ "task": "download", "url": "https://www.data.gouv.fr/fr/datasets/r/49c6f5cd-85b2-4582-a4bb-3208c839f626", "file": "2014_MUN_T1_T2.txt"},
		{ "task": "python", "script": "process_csv.py", "input": "2014_MUN_T1_T2.txt", "output": "out.csv", "delimiter":";", "encoding":"ANSI", "year": "2014", "type_election": "municipales", "nlines": "17", "no_log_combine":"True", "header": "N° tour;Code département;Code commune;Nom de la commune;N° de bureau de vote;Inscrits;Votants;Exprimés;N° de dépôt de la liste;Nom du candidat tête de liste;Prénom du candidat tête de liste;Code nuance de la liste;Nombre de voix" },
		{ "task": "download", "url": "https://www.data.gouv.fr/fr/datasets/r/32981cf5-2560-48c7-a355-1acdf33bd504", "file": "2012_LEG_T1_T2.txt"},
		{ "task": "python", "script": "process_csv.py", "input": "2012_LEG_T1_T2.txt", "output": "out.csv", "delimiter":";", "encoding":"ANSI", "year": "2012", "type_election": "legislatives", "nlines": "18", "no_log_combine":"True", "header": "N° tour;Code département;Code commune;Nom de la commune;N° de circonscription Lg;N° de canton;N° de bureau de vote;Inscrits;Votants;Exprimés;N° de dépôt du candidat;Nom du candidat;Prénom du candidat;Code nuance du candidat;Nombre de voix du candidat" },
		{ "task": "download", "url": "https://www.data.gouv.fr/fr/datasets/r/e9eed534-9ab7-49b2-87a6-53931d0e3512", "file": "2010_REG_T1_T2.txt"},
		{ "task": "python", "script": "process_csv.py", "input": "2010_REG_T1_T2.txt", "output": "out.csv", "delimiter":",", "encoding":"ANSI", "year": "2010", "type_election": "regionales", "nlines": "17", "no_log_combine":"True", "header": "N° tour,Code département,Code commune,Nom de la commune,N° de bureau de vote,Inscrits,Votants,Exprimés,N° de dépôt de la liste,Nom du candidat tête de liste,Prénom du candidat tête de liste,Code nuance de la liste,Nombre de voix" },
		{ "task": "download", "url": "https://www.data.gouv.fr/fr/datasets/r/be1f18cf-c342-419b-879a-37f5312b9735", "file": "2009_EUR_T1_T2.txt"},
		{ "task": "python", "script": "process_csv.py", "input": "2009_EUR_T1_T2.txt", "output": "out.csv", "delimiter":";", "encoding":"ANSI", "year": "2009", "type_election": "europeennes", "nlines": "16", "no_log_combine":"True", "header": "N° tour;Code département;Code commune;Nom de la commune;N° de bureau de vote;Inscrits;Votants;Exprimés;Nom du candidat tête de liste;Prénom du candidat tête de liste;Code nuance de la liste;Nombre de voix" },

		{ "task": "download", "url": "https://www.data.gouv.fr/fr/datasets/r/17fbaf03-33f2-48d6-95e8-aaa13809e432", "file": "2002_PRE_T1_T2.txt"},
		{ "task": "python", "script": "process_csv.py", "input": "2002_PRE_T1_T2.txt", "output": "out.csv", "delimiter":";", "encoding":"ANSI", "year": "2002", "type_election": "presidentielles", "nlines": "17", "no_log_combine":"True", "header": "N° tour;Code département;Code commune;Nom de la commune;N° de bureau de vote;Inscrits;Votants;Exprimés;n° de depot du candidat;Nom du candidat tête de liste;Prénom du candidat;;Nombre de voix" },
		{ "task": "download", "url": "https://www.data.gouv.fr/fr/datasets/r/83e6c566-e313-4dbb-a504-8467c0952697", "file": "2007_PRE_T1_T2.txt"},
		{ "task": "python", "script": "process_csv.py", "input": "2007_PRE_T1_T2.txt", "output": "out.csv", "delimiter":";", "encoding":"ANSI", "year": "2007", "type_election": "presidentielles", "nlines": "17", "no_log_combine":"True", "header": "N° tour;Code département;Code commune;Nom de la commune;N° de bureau de vote;Inscrits;Votants;Exprimés;n° de depot du candidat;Nom du candidat tête de liste;Prénom du candidat;;Nombre de voix" },
		{ "task": "download", "url": "https://www.data.gouv.fr/fr/datasets/r/206b1668-2b31-46da-b957-9857f94fe85c", "file": "2012_PRE_T1_T2.txt"},
		{ "task": "python", "script": "process_csv.py", "input": "2012_PRE_T1_T2.txt", "output": "out.csv", "delimiter":";", "encoding":"ANSI", "year": "2012", "type_election": "presidentielles", "no_log_combine":"True", "header": "tour;code departement;code commune;nom de la commune;;;N° de bureau de vote;Inscrits;Votants;Exprimés;n.pan;nom;prenom;;Nombre de voix" },
		{ "task": "download", "url": "https://www.data.gouv.fr/fr/datasets/r/8fdb0926-ea9d-4fb4-a136-7767cd97e30b", "file": "2017_PRE_T1.txt"},
		{ "task": "python", "script": "process_csv.py", "input": "2017_PRE_T1.txt", "output": "out.csv", "delimiter":";", "encoding":"ANSI", "year": "2017", "type_election": "presidentielles", "tour": "1"},
		{ "task": "download", "url": "https://www.data.gouv.fr/fr/datasets/r/2e3e44de-e584-4aa2-8148-670daf5617e1", "file": "2017_PRES_T2.txt"},
		{ "task": "python", "script": "process_csv.py", "input": "2017_PRES_T2.txt", "output": "out.csv", "delimiter": ";", "encoding":"ANSI", "year": "2017", "type_election": "presidentielles", "tour": "2"},
		{ "task": "download", "url": "https://www.data.gouv.fr/fr/datasets/r/79b5cac4-4957-486b-bbda-322d80868224", "file": "2022_PRE_T1.txt"},
		{ "task": "python", "script": "process_csv.py", "input": "2022_PRE_T1.txt", "output": "out.csv", "delimiter":";", "encoding":"ANSI", "year": "2022", "type_election": "presidentielles", "tour": "1"},
		{ "task": "download", "url": "https://www.data.gouv.fr/fr/datasets/r/4dfd05a9-094e-4043-8a19-43b6b6bbe086", "file": "2022_PRE_T2.txt"},
		{ "task": "python", "script": "process_csv.py", "input": "2022_PRE_T2.txt", "output": "out.csv", "delimiter":";", "encoding":"ANSI", "year": "2022", "type_election": "presidentielles", "tour": "2"},
		
		{ "task": "download", "url": "https://www.data.gouv.fr/fr/datasets/r/a1f73b85-8194-44f4-a2b7-c343edb47d32", "file": "2022_LEG_T1.txt"},
		{ "task": "python", "script": "process_csv.py", "input": "2022_LEG_T1.txt", "output": "out.csv", "delimiter":";", "encoding":"ANSI", "year": "2022", "type_election": "legislatives", "tour": "1"},
		{ "task": "download", "url": "https://www.data.gouv.fr/fr/datasets/r/cada247a-6528-44e7-8308-30c0c335a4b2", "file": "2022_LEG_T2.txt"},
		{ "task": "python", "script": "process_csv.py", "input": "2022_LEG_T2.txt", "output": "out.csv", "delimiter":";", "encoding":"ANSI", "year": "2022", "type_election": "legislatives", "tour": "2"},
		{ "task": "db_import", "file": "out.csv", "format": "csv", "delimiter": ";", "header": true, "table_name": "elections", "lowercase": true, "adminMapping": true},
		{ "task": "sql", "file": "v_elections_stats.sql" },
		{ "task": "sql", "file": "v_elections_candidats.sql" }
	],
	"adminMapping": { "from": ["code_commune"], "to": {"table":"communes","columns":["code"]} }
}
