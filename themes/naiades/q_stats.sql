SELECT
	CASE
		WHEN date_part('month', dateprel) = 1 THEN 'Janvier'
		WHEN date_part('month', dateprel) = 2 THEN 'Février'
		WHEN date_part('month', dateprel) = 3 THEN 'Mars'
		WHEN date_part('month', dateprel) = 4 THEN 'Avril'
		WHEN date_part('month', dateprel) = 5 THEN 'Mai'
		WHEN date_part('month', dateprel) = 6 THEN 'Juin'
		WHEN date_part('month', dateprel) = 7 THEN 'Juillet'
		WHEN date_part('month', dateprel) = 8 THEN 'Août'
		WHEN date_part('month', dateprel) = 9 THEN 'Septembre'
		WHEN date_part('month', dateprel) = 10 THEN 'Octobre'
		WHEN date_part('month', dateprel) = 11 THEN 'Novembre'
		WHEN date_part('month', dateprel) = 12 THEN 'Décembre'
	END as mois,
	avg(<POLLUANT>) AS moyenne
FROM v_naiades
WHERE (<ADMTYPE> = <ADMCODE> or <ADMCODE> is null)
GROUP BY date_part('month', dateprel)
ORDER BY date_part('month', dateprel);