DROP MATERIALIZED VIEW IF EXISTS v_naiades;
CREATE materialized view v_naiades AS
with analyses AS (
	SELECT
		cdstationmesureeauxsurface,
		cdparametre,
		rsana,
		"date",
		x_commune_code,
		x_epci_code,
		x_arrondissement_code,
		x_departement_code,
		x_region_code
	FROM naiades
	WHERE cdparametre = '1340'
		OR cdparametre = '1311'
		OR (cdparametre = '1335' AND cdunitemesure = '169')
), nitrates AS (
	SELECT
		cdstationmesureeauxsurface AS station,
		rsana AS val,
		"date" AS "dateprel",
		x_commune_code,
		x_epci_code,
		x_arrondissement_code,
		x_departement_code,
		x_region_code
	FROM analyses
	WHERE cdparametre = '1340'
), oxygene AS (
	SELECT
		cdstationmesureeauxsurface AS station,
		rsana AS val,
		"date" AS "dateprel",
		x_commune_code,
		x_epci_code,
		x_arrondissement_code,
		x_departement_code,
		x_region_code
	FROM analyses
	WHERE cdparametre = '1311'
), ammonium AS (
	SELECT
		cdstationmesureeauxsurface AS station,
		rsana AS val,
		"date" AS "dateprel",
		x_commune_code,
		x_epci_code,
		x_arrondissement_code,
		x_departement_code,
		x_region_code
	FROM analyses
	WHERE cdparametre = '1335'
) SELECT
	COALESCE(nitrates.station, oxygene.station, ammonium.station) AS station,
	COALESCE(nitrates.dateprel, oxygene.dateprel, ammonium.dateprel) AS dateprel,
	nitrates.val::float AS nitrates,
	oxygene.val::float AS oxygene,
	ammonium.val::float AS ammonium,
	COALESCE(nitrates.x_commune_code, oxygene.x_commune_code, ammonium.x_commune_code) x_commune_code,
	COALESCE(nitrates.x_epci_code, oxygene.x_epci_code, ammonium.x_epci_code) x_epci_code,
	COALESCE(nitrates.x_arrondissement_code, oxygene.x_arrondissement_code, ammonium.x_arrondissement_code) x_arrondissement_code,
	COALESCE(nitrates.x_departement_code, oxygene.x_departement_code, ammonium.x_departement_code) x_departement_code,
	COALESCE(nitrates.x_region_code, oxygene.x_region_code, ammonium.x_region_code) x_region_code
FROM nitrates
	FULL JOIN oxygene ON (nitrates.station = oxygene.station and nitrates.dateprel = oxygene.dateprel)
	FULL JOIN ammonium ON (ammonium.station = nitrates.station and ammonium.dateprel = nitrates.dateprel);

CREATE INDEX "v_naiades.v_naiades_x_commune_code_idx" ON v_naiades (x_commune_code);
CREATE INDEX "v_naiades.v_naiades_x_epci_code_idx" ON v_naiades (x_epci_code);
CREATE INDEX "v_naiades.v_naiades_x_arrondissement_code_idx" ON v_naiades (x_arrondissement_code);
CREATE INDEX "v_naiades.v_naiades_x_departement_code_idx" ON v_naiades (x_departement_code);
CREATE INDEX "v_naiades.v_naiades_x_region_code_idx" ON v_naiades (x_region_code);