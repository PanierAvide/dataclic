Note sur l'exploitation des résultats:

Un même paramètre peut être mesuré selon différentes méthodes ce qui pourrait expliquer certaines caractéristiques du résultat de l'analyse (précision, incertitude). Plus d’informations sur la validation et la qualité des données [Naïades](https://naiades.eaufrance.fr/validation-donnees).