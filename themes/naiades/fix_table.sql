ALTER TABLE naiades
	ADD COLUMN date TIMESTAMP,
	ADD COLUMN x_commune_code VARCHAR,
	ADD COLUMN x_commune_name VARCHAR,
	ADD COLUMN x_epci_code VARCHAR,
	ADD COLUMN x_epci_name VARCHAR,
	ADD COLUMN x_arrondissement_code VARCHAR,
	ADD COLUMN x_arrondissement_name VARCHAR,
	ADD COLUMN x_departement_code VARCHAR,
	ADD COLUMN x_departement_name VARCHAR,
	ADD COLUMN x_region_code VARCHAR,
	ADD COLUMN x_region_name VARCHAR,
	ADD COLUMN geom GEOMETRY(POINT, 4326)
;

UPDATE naiades AS a SET
	"date" = to_timestamp(concat_ws('_', dateprel, COALESCE(heureprel, '00:00:00')), 'YYYY-MM-DD_HH24:MI:SS')::timestamp without time zone at time zone 'Europe/Paris',
	geom = o.geom,
	x_commune_code = o.x_commune_code,
	x_commune_name = o.x_commune_name,
	x_epci_code = o.x_epci_code,
	x_epci_name = o.x_epci_name,
	x_arrondissement_code = o.x_arrondissement_code,
	x_arrondissement_name = o.x_arrondissement_name,
	x_departement_code = o.x_departement_code,
	x_departement_name = o.x_departement_name,
	x_region_code = o.x_region_code,
	x_region_name = o.x_region_name
FROM (
	WITH op AS (
		SELECT
			cdstationmesureeauxsurface,
			ST_Transform(ST_SetSRID(ST_MakePoint(coordxprel::float, coordyprel::float), 2154), 4326) AS geom
		FROM naiades_operations_2021
        GROUP BY cdstationmesureeauxsurface, geom
	)
	SELECT
		cdstationmesureeauxsurface,
		geom,
		code AS x_commune_code,
		name AS x_commune_name,
		epci  AS x_epci_code,
		epci_name AS x_epci_name,
		arrondissement AS x_arrondissement_code,
		arrondissement_name AS x_arrondissement_name,
		departement AS x_departement_code,
		departement_name AS x_departement_name,
		region AS x_region_code,
		region_name AS x_region_name
	FROM op
	JOIN communes on ST_Contains(geo_boundary, geom)
) o
WHERE a.cdstationmesureeauxsurface = o.cdstationmesureeauxsurface ;

DROP TABLE naiades_operations_2021;

DELETE FROM naiades WHERE
	x_commune_code IS NULL
	AND x_epci_code IS NULL
	AND x_departement_code IS NULL
	AND x_region_code IS NULL;

CREATE INDEX "naiades.naiades_x_commune_code_idx" on naiades (x_commune_code);
CREATE INDEX "naiades.naiades_x_epci_code_idx" on naiades (x_epci_code);
CREATE INDEX "naiades.naiades_x_arrondissement_code_idx" on naiades (x_arrondissement_code);
CREATE INDEX "naiades.naiades_x_departement_code_idx" on naiades (x_departement_code);
CREATE INDEX "naiades.naiades_x_region_code_idx" on naiades (x_region_code);