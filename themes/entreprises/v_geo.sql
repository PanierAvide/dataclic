create materialized view v_geo_entreprises as
	select
		siret,
		COALESCE("denominationUsuelleEtablissement", CONCAT_WS(' ', "enseigne1Etablissement", "enseigne2Etablissement", "enseigne3Etablissement")) as name,
		x_commune_code,
		x_epci_code,
		x_arrondissement_code,
		x_departement_code,
		x_region_code,
		ST_POINT(longitude::float, latitude::float)::geometry(point, 4326) as geom
	from entreprises
	where "etatAdministratifEtablissement" = 'A' and geo_score::float >= 0.80 
	;

create index "v_entreprises_geo.geom" on v_geo_entreprises USING gist (geom);
create index "v_geo_entreprises.v_geo_entreprises_x_commune_code_idx" on v_geo_entreprises (x_commune_code);
create index "v_geo_entreprises.v_geo_entreprises_x_epci_code_idx" on v_geo_entreprises (x_epci_code);
create index "v_geo_entreprises.v_geo_entreprises_x_arrondissement_code_idx" on v_geo_entreprises (x_arrondissement_code);
create index "v_geo_entreprises.v_geo_entreprises_x_departement_code_idx" on v_geo_entreprises (x_departement_code);
create index "v_geo_entreprises.v_geo_entreprises_x_region_code_idx" on v_geo_entreprises (x_region_code);

CLUSTER v_geo_entreprises USING "v_entreprises_geo.geom";
