DROP MATERIALIZED VIEW IF EXISTS v_entreprises_code_naf;
CREATE MATERIALIZED VIEW v_entreprises_code_naf AS
	SELECT
		CASE
			WHEN n.libelle_section IS NULL THEN 'Inconnue   '
			ELSE FORMAT('%s (%s)   ', n.libelle_section, n.code_section)
		END AS libelle_section,
		count(*) AS n_entreprises,
		e.x_commune_code,
		e.x_epci_code,
		e.x_arrondissement_code,
		e.x_departement_code,
		e.x_region_code 
	FROM entreprises e 
	LEFT JOIN
		code_naf n on n.code_sous_classe  = e."activitePrincipaleEtablissement"
	WHERE "etatAdministratifEtablissement" = 'A'
	GROUP BY 
		x_commune_code, 
		n.libelle_section,
		n.code_section,
		x_epci_code,
		x_arrondissement_code,
		x_departement_code,
		x_region_code
	;


CREATE INDEX "v_entreprises_code_naf.v_entreprises_code_naf_x_commune_code_idx" ON v_entreprises_code_naf (x_commune_code);
CREATE INDEX "v_entreprises_code_naf.v_entreprises_code_naf_x_epci_code_idx" ON v_entreprises_code_naf (x_epci_code);
CREATE INDEX "v_entreprises_code_naf.v_entreprises_code_naf_x_arrondissement_code_idx" ON v_entreprises_code_naf (x_arrondissement_code);
CREATE INDEX "v_entreprises_code_naf.v_entreprises_code_naf_x_departement_code_idx" ON v_entreprises_code_naf (x_departement_code);
CREATE INDEX "v_entreprises_code_naf.v_entreprises_code_naf_x_region_code_idx" ON v_entreprises_code_naf (x_region_code);