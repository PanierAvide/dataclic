select
	libelle_section as x,
	sum(n_entreprises) as y
from v_entreprises_code_naf
where
	(<ADMTYPE> = <ADMCODE> or <ADMCODE> is null)
group by libelle_section
order by y desc;