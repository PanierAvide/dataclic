drop materialized view if exists v_geo_qualite_air;
create materialized view v_geo_qualite_air as
	select distinct on (geom) 
		geom,
		date_ech,
		case
			when lower(lib_qual) = 'bon' or lower(unaccent(lib_qual)) = 'tres bon' then '1'
			when lower(lib_qual) = 'moyen' then '2'
			when lower(unaccent(lib_qual)) = 'degrade' then '3'
			when lower(lib_qual) = 'mauvais' then '4'
			when lower(unaccent(lib_qual)) = 'tres mauvais' then '5'
			when lower(unaccent(lib_qual)) = 'extremement mauvais' or code_qual::int >= 6 then '6'
			else '0'
		end as code_qual,
		source,
		x_commune_code,
		x_epci_code,
		x_departement_code,
		x_region_code
	from qualite_air
	where date_ech = CURRENT_DATE::VARCHAR
	order by geom, date_ech desc
	;



create index "v_geo_qualite_air.geom_idx" on v_geo_qualite_air USING gist (geom);
CLUSTER v_geo_qualite_air USING "v_geo_qualite_air.geom_idx";