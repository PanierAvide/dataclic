DROP materialized view if exists v_geo_qualite_air;
CREATE TABLE IF NOT EXISTS qualite_air (
	fid varchar NULL,
	date_maj varchar NULL,
	the_geom varchar NULL,
	code_no2 varchar NULL,
	code_o3 varchar NULL,
	code_pm10 varchar NULL,
	code_pm25 varchar NULL,
	code_qual varchar NULL,
	code_so2 varchar NULL,
	code_zone varchar NULL,
	coul_qual varchar NULL,
	date_dif varchar NULL,
	date_ech varchar NULL,
	epsg_reg varchar NULL,
	lib_qual varchar NULL,
	lib_zone varchar NULL,
	"source" varchar NULL,
	type_zone varchar NULL,
	x_reg varchar NULL,
	x_wgs84 varchar NULL,
	y_reg varchar NULL,
	y_wgs84 varchar NULL
);

ALTER TABLE qualite_air
DROP COLUMN IF EXISTS x_commune_code,
DROP COLUMN IF EXISTS x_commune_name,
DROP COLUMN IF EXISTS x_epci_code,
DROP COLUMN IF EXISTS x_epci_name,
DROP COLUMN IF EXISTS x_arrondissement_code,
DROP COLUMN IF EXISTS x_arrondissement_name,
DROP COLUMN IF EXISTS x_departement_code,
DROP COLUMN IF EXISTS x_departement_name,
DROP COLUMN IF EXISTS x_region_code,
DROP COLUMN IF EXISTS x_region_name;