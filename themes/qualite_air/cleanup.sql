DELETE FROM 
	qualite_air a
USING qualite_air b
WHERE
	(
		a.date_dif < b.date_dif
		AND a.the_geom = b.the_geom
		AND a.code_zone = b.code_zone
		AND a.coul_qual = b.coul_qual
		AND a.date_ech = b.date_ech
		AND a.epsg_reg = b.epsg_reg
		AND a.lib_zone = b.lib_zone
		AND a."source" = b."source"
		AND a.type_zone = b.type_zone
		AND a.x_reg = b.x_reg
		AND a.x_wgs84 = b.x_wgs84
		AND a.y_reg = b.y_reg
		AND a.y_wgs84 = b.y_wgs84
	);

DELETE
FROM qualite_air 
WHERE TO_DATE(date_ech, 'YYYY-MM-DD') < (CURRENT_DATE - INTERVAL '30 DAY');