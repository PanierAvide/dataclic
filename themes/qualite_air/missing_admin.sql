ALTER TABLE qualite_air
    ADD COLUMN IF NOT EXISTS geom geometry(POINT, 4326);


UPDATE qualite_air
SET geom =  case
                when (x_wgs84 is not null) and (y_wgs84 is not null) then ST_SetSRID(ST_MakePoint(x_wgs84::float, y_wgs84::float), 4326)
                when (x_reg is not null) and (y_reg is not null) and (epsg_reg is not null) then ST_Transform(ST_SetSRID(ST_MakePoint(x_reg::float, y_reg::float), epsg_reg::int), 4326)
		    end
WHERE geom IS NULL;

CREATE INDEX IF NOT EXISTS "qualite_air.geom_idx" on qualite_air USING gist (geom);

UPDATE qualite_air q
SET x_commune_code = c.code,
    x_commune_name = c.name,
    x_epci_code = c.epci,
    x_epci_name = c.epci_name,
    x_arrondissement_code = c.arrondissement,
    x_arrondissement_name = c.arrondissement_name,
    x_departement_code = c.departement,
    x_departement_name = c.departement_name,
    x_region_code = c.region,
    x_region_name = c.region_name
FROM communes c
WHERE x_commune_code IS NULL
    AND c.geo_boundary && geom AND ST_Intersects(c.geo_boundary, geom)
;