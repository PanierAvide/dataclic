select
    '' as "year",
	sum(p08_pop)::int as "pop_2008",
	sum(p13_pop)::int as "pop_2013",
	sum(p19_pop)::int as "pop_2019"
from recensement_population
where (<ADMTYPE> = <ADMCODE> or <ADMCODE> is null)
;