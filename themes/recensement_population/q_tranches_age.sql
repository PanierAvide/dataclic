select
	'' as year,
	sum(p19_pop0014) as "p_0014",
	sum(p19_pop1529) as "p_1529",
	sum(p19_pop3044) as "p_3044",
	sum(p19_pop4559) as "p_4559",
	sum(p19_pop6074) as "p_6074",
	sum(p19_pop7589) as "p_7589",
	sum(p19_pop90p) as "90p"
from recensement_population
where (<ADMTYPE> = <ADMCODE> or <ADMCODE> is null)
;